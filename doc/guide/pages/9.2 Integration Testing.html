<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <title>9.2 Integration Testing</title>
        <link rel="stylesheet" href="../../css/main.css" type="text/css" media="screen" title="Ref" charset="utf-8"/>
        <link rel="icon" href="../../img/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon"/>
    </head>
    <body class="body">
        <div id="header">
            <h2><a name="9.2 Integration Testing">9.2 Integration Testing</a></h2>
        </div>
        <div id="toc">
            
        </div>
        <div id="content">
            Integration tests differ from unit tests in that you have full access to the Grails environment within the test. Grails will use an in-memory HSQLDB database for integration tests and clear out all the data from the database in between each test.<p class="paragraph"/>One thing to bear in mind is that logging is enabled for your application classes, but that is different from logging in tests. So if you have something like this:<p class="paragraph"/><div class="code"><pre>class MyServiceTests <span class="java&#45;keyword">extends</span> GroovyTestCase &#123;
    void testSomething() &#123;
        log.info <span class="java&#45;quote">"Starting tests"</span>
        &#8230;
    &#125;
&#125;</pre></div><p class="paragraph"/>the "starting tests" message is logged using a different system to the one used by the application. Basically the <code>log</code> property in the example above is an instance of <code>java.util.logging.Logger</code>, which doesn't have exactly the same methods as the <code>log</code> property injected into your application artifacts. For example, it doesn't have <code>debug()</code> or <code>trace()</code> methods, and the equivalent of <code>warn()</code> is in fact <code>warning()</code>.<p class="paragraph"/><h4>Transactions</h4><p class="paragraph"/>The integration tests run inside a database transaction by default, which is then rolled back at the end of the tests. This means that data saved during the tests is not persisted to the database. If you actually want to check transactional behaviour of your services and controllers, then you can disable a test's transaction by adding a <code>transactional</code> property to your test case:<p class="paragraph"/><div class="code"><pre>class MyServiceTests <span class="java&#45;keyword">extends</span> GroovyTestCase &#123;
    <span class="java&#45;keyword">static</span> transactional = <span class="java&#45;keyword">false</span><p class="paragraph"/>    void testMyTransactionalServiceMethod() &#123;
        &#8230;
    &#125;
&#125;</pre></div><p class="paragraph"/>
<h4>Testing Controllers</h4><p class="paragraph"/>To test controllers you first have to understand the Spring Mock Library.<p class="paragraph"/>Essentially Grails automatically configures each test with a <a href="http://static.springsource.org/spring/docs/3.0.x/javadoc-api/org/springframework/mock/web/MockHttpServletRequest.html" class="api">MockHttpServletRequest</a>, <a href="http://static.springsource.org/spring/docs/3.0.x/javadoc-api/org/springframework/mock/web/MockHttpServletResponse.html" class="api">MockHttpServletResponse</a>, and <a href="http://static.springsource.org/spring/docs/3.0.x/javadoc-api/org/springframework/mock/web/MockHttpSession.html" class="api">MockHttpSession</a> which you can then use to perform your tests. For example consider the following controller:<p class="paragraph"/><div class="code"><pre>class FooController &#123;<p class="paragraph"/>    def text = &#123;
        render <span class="java&#45;quote">"bar"</span>
    &#125;<p class="paragraph"/>    def someRedirect = &#123;
        redirect(action:<span class="java&#45;quote">"bar"</span>)
    &#125;
&#125;</pre></div><p class="paragraph"/>The tests for this would be:<p class="paragraph"/><div class="code"><pre>class FooControllerTests <span class="java&#45;keyword">extends</span> GroovyTestCase &#123;<p class="paragraph"/>    void testText() &#123;
        def fc = <span class="java&#45;keyword">new</span> FooController()
        fc.text()
        assertEquals <span class="java&#45;quote">"bar"</span>, fc.response.contentAsString
    &#125;<p class="paragraph"/>    void testSomeRedirect() &#123;<p class="paragraph"/>        def fc = <span class="java&#45;keyword">new</span> FooController()
        fc.someRedirect()
        assertEquals <span class="java&#45;quote">"/foo/bar"</span>, fc.response.redirectedUrl
    &#125;
&#125;</pre></div><p class="paragraph"/>In the above case the response is an instance of <code>MockHttpServletResponse</code> which we can use to obtain the <code>contentAsString</code> (when writing to the response) or the URL redirected to for example. These mocked versions of the Servlet API are, unlike the real versions, all completely mutable and hence you can set properties on the request such as the <code>contextPath</code> and so on.<p class="paragraph"/>Grails <strong class="bold">does not</strong> invoke <a href="../guide/single.html#6.1.5 Controller Interceptors" class="guide">interceptors</a> or servlet filters automatically when calling actions during integration testing. You should test interceptors and filters in isolation, and via <a href="../guide/single.html#9.3 Functional Testing" class="guide">functional testing</a> if necessary.<p class="paragraph"/><h4>Testing Controllers with Services</h4><p class="paragraph"/>If your controller references a service (or other Spring beans), you have to explicitly initialise the service from your test.<p class="paragraph"/>Given a controller using a service:<p class="paragraph"/><div class="code"><pre>class FilmStarsController &#123;
    def popularityService<p class="paragraph"/>    def update = &#123;
        // <span class="java&#45;keyword">do</span> something with popularityService
    &#125;
&#125;</pre></div><p class="paragraph"/>The test for this would be:<p class="paragraph"/><div class="code"><pre>class FilmStarsTests <span class="java&#45;keyword">extends</span> GroovyTestCase &#123;
    def popularityService<p class="paragraph"/>    void testInjectedServiceInController () &#123;
        def fsc = <span class="java&#45;keyword">new</span> FilmStarsController()
        fsc.popularityService = popularityService
        fsc.update()
    &#125;
&#125;</pre></div><p class="paragraph"/><h4>Testing Controller Command Objects</h4><p class="paragraph"/>With command objects you just supply parameters to the request and it will automatically do the command object work for you when you call your action with no parameters:<p class="paragraph"/>Given a controller using a command object:<p class="paragraph"/><div class="code"><pre>class AuthenticationController &#123;
    def signup = &#123; SignupForm form &#45;&#62;
        &#8230;
    &#125;
&#125;</pre></div><p class="paragraph"/>You can then test it like this:<p class="paragraph"/><div class="code"><pre>def controller = <span class="java&#45;keyword">new</span> AuthenticationController()
controller.params.login = <span class="java&#45;quote">"marcpalmer"</span>
controller.params.password = <span class="java&#45;quote">"secret"</span>
controller.params.passwordConfirm = <span class="java&#45;quote">"secret"</span>
controller.signup()</pre></div><p class="paragraph"/>Grails auto-magically sees your call to <code>signup()</code> as a call to the action and populates the command object from the mocked request parameters. During controller testing, the <code>params</code> are mutable with a mocked request supplied by Grails.<p class="paragraph"/><h4>Testing Controllers and the render Method</h4><p class="paragraph"/>The <a href="../ref/Controllers/render.html" class="controllers">render</a> method allows you to render a custom view at any point within the body of an action. For instance, consider the example below:<p class="paragraph"/><div class="code"><pre>def save = &#123;
    def book = Book(params)
    <span class="java&#45;keyword">if</span>(book.save()) &#123;
        // handle
    &#125;
    <span class="java&#45;keyword">else</span> &#123;
        render(view:<span class="java&#45;quote">"create"</span>, model:&#91;book:book&#93;)
    &#125;
&#125;</pre></div><p class="paragraph"/>In the above example the result of the model of the action is not available as the return value, but instead is stored within the <code>modelAndView</code> property of the controller. The <code>modelAndView</code> property is an instance of Spring MVC's <a href="http://static.springsource.org/spring/docs/3.0.x/javadoc-api/org/springframework/web/servlet/ModelAndView.html" target="blank">ModelAndView</a> class and you can use it to the test the result of an action:<p class="paragraph"/><div class="code"><pre>def bookController = <span class="java&#45;keyword">new</span> BookController()
bookController.save()
def model = bookController.modelAndView.model.book</pre></div><p class="paragraph"/><h4>Simulating Request Data</h4><p class="paragraph"/>If you're testing an action that requires request data such as a REST web service you can use the Spring <a href="http://static.springsource.org/spring/docs/3.0.x/javadoc-api/org/springframework/mock/web/MockHttpServletRequest.html" class="api">MockHttpServletRequest</a> object to do so. For example consider this action which performs data binding from an incoming request:<p class="paragraph"/><div class="code"><pre>def create = &#123;
    &#91;book: <span class="java&#45;keyword">new</span> Book(params&#91;'book'&#93;) &#93;
&#125;</pre></div><p class="paragraph"/>If you wish the simulate the 'book' parameter as an XML request you could do something like the following:<p class="paragraph"/><div class="code"><pre>void testCreateWithXML() &#123;
    def controller = <span class="java&#45;keyword">new</span> BookController()
    controller.request.contentType = 'text/xml'
    controller.request.content = '''&#60;?xml version=<span class="java&#45;quote">"1.0"</span> encoding=<span class="java&#45;quote">"ISO&#45;8859&#45;1"</span>?&#62;
    &#60;book&#62;
        &#60;title&#62;The Stand&#60;/title&#62;
        &#8230;
    &#60;/book&#62;
    '''.getBytes() // note we need the bytes<p class="paragraph"/>    def model = controller.create()
    assert model.book
    assertEquals <span class="java&#45;quote">"The Stand"</span>, model.book.title
&#125;</pre></div><p class="paragraph"/>The same can be achieved with a JSON request:<p class="paragraph"/><div class="code"><pre>void testCreateWithJSON() &#123;
    def controller = <span class="java&#45;keyword">new</span> BookController()
    controller.request.contentType = <span class="java&#45;quote">"text/json"</span>
    controller.request.content = '&#123;<span class="java&#45;quote">"id"</span>:1,<span class="java&#45;quote">"class"</span>:<span class="java&#45;quote">"Book"</span>,<span class="java&#45;quote">"title"</span>:<span class="java&#45;quote">"The Stand"</span>&#125;'.getBytes()<p class="paragraph"/>    def model = controller.create()
    assert model.book
    assertEquals <span class="java&#45;quote">"The Stand"</span>, model.book.title
&#125;</pre></div><p class="paragraph"/><blockquote class="note">
With JSON don't forget the <code>class</code> property to specify the name the target type to bind too. In the XML this is implicit within the name of the <code>&#60;book&#62;</code> node, but with JSON you need this property as part of the JSON packet.
</blockquote><p class="paragraph"/>For more information on the subject of REST web services see the section on <a href="../guide/single.html#13.1 REST" class="guide">REST</a>.<p class="paragraph"/><h4>Testing Web Flows</h4><p class="paragraph"/>Testing <a href="../guide/single.html#6.5 Web Flow" class="guide">Web Flows</a> requires a special test harness called <code>grails.test.WebFlowTestCase</code> which sub classes Spring Web Flow's <a href="http://static.springsource.org/spring/docs/3.0.x/javadoc-api/org/springframework/webflow/test/execution/AbstractFlowExecutionTests.html" class="api">AbstractFlowExecutionTests</a> class.<p class="paragraph"/><blockquote class="note">
Subclasses of <code>WebFlowTestCase</code> <strong class="bold">must</strong> be integration tests
</blockquote><p class="paragraph"/>For example given this trivial flow:<p class="paragraph"/><div class="code"><pre>class ExampleController &#123;
    def exampleFlow = &#123;
        start &#123;
            on(<span class="java&#45;quote">"go"</span>) &#123;
                flow.hello = <span class="java&#45;quote">"world"</span>
            &#125;.to <span class="java&#45;quote">"next"</span>
        &#125;
        next &#123;
            on(<span class="java&#45;quote">"back"</span>).to <span class="java&#45;quote">"start"</span>
            on(<span class="java&#45;quote">"go"</span>).to <span class="java&#45;quote">"subber"</span>
        &#125;
        subber &#123;
            subflow(action: <span class="java&#45;quote">"sub"</span>)
            on(<span class="java&#45;quote">"end"</span>).to(<span class="java&#45;quote">"end"</span>)
        &#125;
        end()
    &#125;<p class="paragraph"/>    def subFlow = &#123;
        subSubflowState &#123;
            subflow(controller: <span class="java&#45;quote">"other"</span>, action: <span class="java&#45;quote">"otherSub"</span>)
            on(<span class="java&#45;quote">"next"</span>).to(<span class="java&#45;quote">"next"</span>)
        &#125;
        &#8230;
    &#125;
&#125;</pre></div><p class="paragraph"/>You need to tell the test harness what to use for the "flow definition". This is done via overriding the abstract <code>getFlow</code>
 method:<p class="paragraph"/><div class="code"><pre>class ExampleFlowTests <span class="java&#45;keyword">extends</span> grails.test.WebFlowTestCase &#123;
    def getFlow() &#123; <span class="java&#45;keyword">new</span> ExampleController().exampleFlow &#125;
    &#8230;
&#125;</pre></div><p class="paragraph"/>If you need to specify the flow id you can do so by overriding the getFlowId method otherwise the default is <code>test</code>:
<div class="code"><pre>class ExampleFlowTests <span class="java&#45;keyword">extends</span> grails.test.WebFlowTestCase &#123;
    <span class="java&#45;object">String</span> getFlowId() &#123; <span class="java&#45;quote">"example"</span> &#125;
    &#8230;
&#125;</pre></div><p class="paragraph"/>If the flow under test calls any subflows, these (or mocks) need to be registered before the calling flow :
<div class="code"><pre><span class="java&#45;keyword">protected</span> void setUp() &#123;
    <span class="java&#45;keyword">super</span>.setUp()
    registerFlow(<span class="java&#45;quote">"other/otherSub"</span>) &#123; // register a simplified mock
        start &#123;
            on(<span class="java&#45;quote">"next"</span>).to(<span class="java&#45;quote">"end"</span>)
        &#125;
        end()
    &#125;
    registerFlow(<span class="java&#45;quote">"example/sub"</span>, <span class="java&#45;keyword">new</span> ExampleController().subFlow) // register the original subflow
&#125;</pre></div><p class="paragraph"/>Once this is done in your test you need to kick off the flow with the <code>startFlow</code> method:<p class="paragraph"/><div class="code"><pre>void testExampleFlow() &#123;
    def viewSelection = startFlow()
    &#8230;
&#125;</pre></div><p class="paragraph"/>To trigger and event you need to use the <code>signalEvent</code> method:<p class="paragraph"/><div class="code"><pre>void testExampleFlow() &#123;
    &#8230;
    signalEvent(<span class="java&#45;quote">"go"</span>)
    assert <span class="java&#45;quote">"next"</span> == flowExecution.activeSession.state.id
    assert <span class="java&#45;quote">"world"</span> == flowScope.hello
&#125;</pre></div><p class="paragraph"/>Here we have signaled to the flow to execute the event "go" this causes a transition to the "next" state. In the example a transition action placed a <code>hello</code> variable into the flow scope.<p class="paragraph"/>
<h4>Testing Tag Libraries</h4><p class="paragraph"/>Testing tag libraries is actually pretty trivial because when a tag is invoked as a method it returns its result as a string. So for example if you have a tag library like this:<p class="paragraph"/><div class="code"><pre>class FooTagLib &#123;
   def bar =  &#123; attrs, body &#45;&#62;
          out &#60;&#60; <span class="java&#45;quote">"&#60;p&#62;Hello World!&#60;/p&#62;"</span>
   &#125;<p class="paragraph"/>   def bodyTag =  &#123; attrs, body &#45;&#62;
      out &#60;&#60; <span class="java&#45;quote">"&#60;$&#123;attrs.name&#125;&#62;"</span>
           out &#60;&#60; body()
      out &#60;&#60; <span class="java&#45;quote">"&#60;/$&#123;attrs.name&#125;&#62;"</span>
   &#125;
&#125;</pre></div><p class="paragraph"/>The tests would look like:<p class="paragraph"/><div class="code"><pre>class FooTagLibTests <span class="java&#45;keyword">extends</span> GroovyTestCase &#123;<p class="paragraph"/>    void testBarTag() &#123;
        assertEquals <span class="java&#45;quote">"&#60;p&#62;Hello World!&#60;/p&#62;"</span>, <span class="java&#45;keyword">new</span> FooTagLib().bar(<span class="java&#45;keyword">null</span>,<span class="java&#45;keyword">null</span>).toString()
    &#125;<p class="paragraph"/>    void testBodyTag() &#123;
        assertEquals <span class="java&#45;quote">"&#60;p&#62;Hello World!&#60;/p&#62;"</span>, <span class="java&#45;keyword">new</span> FooTagLib().bodyTag(name:<span class="java&#45;quote">"p"</span>) &#123;
            <span class="java&#45;quote">"Hello World!"</span>
        &#125;.toString()
    &#125;
&#125;</pre></div><p class="paragraph"/>Notice that for the second example, <code>testBodyTag</code>, we pass a block that returns the body of the tag. This is handy for representing the body as a String.<p class="paragraph"/><h4>Testing Tag Libraries with GroovyPagesTestCase</h4><p class="paragraph"/>In addition to doing simply testing of tag libraries like the above you can also use the <code>grails.test.GroovyPagesTestCase</code> class to test tag libraries.<p class="paragraph"/>The <code>GroovyPagesTestCase</code> class is a sub class of the regular <code>GroovyTestCase</code> class and provides utility methods for testing the output of a GSP rendering.<p class="paragraph"/><blockquote class="note">
<code>GroovyPagesTestCase</code> can only be used in an integration test.
</blockquote><p class="paragraph"/>As an example given a date formatting tag library such as the one below:<p class="paragraph"/><div class="code"><pre>class FormatTagLib &#123;
    def dateFormat = &#123; attrs, body &#45;&#62;
        out &#60;&#60; <span class="java&#45;keyword">new</span> java.text.SimpleDateFormat(attrs.format) &#60;&#60; attrs.date
    &#125;
&#125;</pre></div><p class="paragraph"/>This can be easily tested as follows:<p class="paragraph"/><div class="code"><pre>class FormatTagLibTests <span class="java&#45;keyword">extends</span> GroovyPagesTestCase &#123;
    void testDateFormat() &#123;
        def template = '&#60;g:dateFormat format=<span class="java&#45;quote">"dd&#45;MM&#45;yyyy"</span> date=<span class="java&#45;quote">"$&#123;myDate&#125;"</span> /&#62;'<p class="paragraph"/>        def testDate = &#8230; // create the date
        assertOutputEquals( '01&#45;01&#45;2008', template, &#91;myDate:testDate&#93; )
    &#125;
&#125;</pre></div><p class="paragraph"/>You can also obtain the result of a GSP using the <code>applyTemplate</code> method of the <code>GroovyPagesTestCase</code> class:<p class="paragraph"/><div class="code"><pre>class FormatTagLibTests <span class="java&#45;keyword">extends</span> GroovyPagesTestCase &#123;
    void testDateFormat() &#123;
        def template = '&#60;g:dateFormat format=<span class="java&#45;quote">"dd&#45;MM&#45;yyyy"</span> date=<span class="java&#45;quote">"$&#123;myDate&#125;"</span> /&#62;'<p class="paragraph"/>        def testDate = &#8230; // create the date
        def result = applyTemplate( template, &#91;myDate:testDate&#93; )<p class="paragraph"/>        assertEquals '01&#45;01&#45;2008', result
    &#125;
&#125;</pre></div><p class="paragraph"/><h4>Testing Domain Classes</h4><p class="paragraph"/>Testing domain classes is typically a simple matter of using the <a href="../guide/single.html#5. Object Relational Mapping (GORM)" class="guide">GORM API</a>, however there are some things to be aware of. Firstly, if you are testing queries you will often need to "flush" in order to ensure the correct state has been persisted to the database. For example take the following example:<p class="paragraph"/><div class="code"><pre>void testQuery() &#123;
    def books = &#91; <span class="java&#45;keyword">new</span> Book(title:<span class="java&#45;quote">"The Stand"</span>), <span class="java&#45;keyword">new</span> Book(title:<span class="java&#45;quote">"The Shining"</span>)&#93;
    books&#42;.save()<p class="paragraph"/>    assertEquals 2, Book.list().size()
&#125;</pre></div><p class="paragraph"/>This test will actually fail, because calling <a href="../ref/Domain Classes/save.html" class="domainClasses">save</a> does not actually persist the <code>Book</code> instances when called. Calling <code>save</code> merely indicates to Hibernate that at some point in the future these instances should be persisted. If you wish to commit changes immediately you need to "flush" them:<p class="paragraph"/><div class="code"><pre>void testQuery() &#123;
    def books = &#91; <span class="java&#45;keyword">new</span> Book(title:<span class="java&#45;quote">"The Stand"</span>), <span class="java&#45;keyword">new</span> Book(title:<span class="java&#45;quote">"The Shining"</span>)&#93;
    books&#42;.save(flush:<span class="java&#45;keyword">true</span>)<p class="paragraph"/>    assertEquals 2, Book.list().size()
&#125;</pre></div><p class="paragraph"/>In this case since we're passing the argument <code>flush</code> with a value of <code>true</code> the updates will be persisted immediately and hence will be available to the query later on.

        </div>
    </body>
</html>
