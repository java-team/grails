<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <title>3.1.2 Logging</title>
        <link rel="stylesheet" href="../../css/main.css" type="text/css" media="screen" title="Ref" charset="utf-8"/>
        <link rel="icon" href="../../img/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon"/>
    </head>
    <body class="body">
        <div id="header">
            <h2><a name="3.1.2 Logging">3.1.2 Logging</a></h2>
        </div>
        <div id="toc">
            
        </div>
        <div id="content">
            <h3>The Basics</h3><p class="paragraph"/>Grails uses its common configuration mechanism to provide the settings for the underlying <a href="http://logging.apache.org/log4j/1.2/index.html" target="blank">Log4j</a> log system, so all you have to do is add a <code>log4j</code> setting to the file <code>grails-app/conf/Config.groovy</code>.<p class="paragraph"/>So what does this <code>log4j</code> setting look like? Here's a basic example:<p class="paragraph"/><div class="code"><pre>log4j = &#123;
    error  'org.codehaus.groovy.grails.web.servlet',  //  controllers
           'org.codehaus.groovy.grails.web.pages' //  GSP<p class="paragraph"/>    warn   'org.mortbay.log'
&#125;</pre></div><p class="paragraph"/>This says that for the two 'org.codehaus.groovy.*' loggers, only messages logged at 'error' level and above will be shown. The 'org.mortbay.log' logger also shows messages at the 'warn' level. What does that mean? First of all, you have to understand how levels work.<p class="paragraph"/><h4>Logging levels</h4><p class="paragraph"/>The are several standard logging levels, which are listed here in order of descending priority:
<ol>
<li>off</li>
<li>fatal</li>
<li>error</li>
<li>warn</li>
<li>info</li>
<li>debug</li>
<li>trace</li>
<li>all</li>
</ol><p class="paragraph"/>When you log a message, you implicitly give that message a level. For example, the method <code>log.error(msg)</code> will log a message at the 'error' level. Likewise, <code>log.debug(msg)</code> will log it at 'debug'. Each of the above levels apart from 'off' and 'all' have a corresponding log method of the same name.<p class="paragraph"/>The logging system uses that  <em class="italic">message</em>  level combined with the configuration for the logger (see next section) to determine whether the message gets written out. For example, if you have an 'org.example.domain' logger configured like so:<p class="paragraph"/><div class="code"><pre>warn 'org.example.domain'</pre></div><p class="paragraph"/>then messages with a level of 'warn', 'error', or 'fatal' will be written out. Messages at other levels will be ignored.<p class="paragraph"/>Before we go on to loggers, a quick note about those 'off' and 'all' levels. These are special in that they can only be used in the configuration; you can't log messages at these levels. So if you configure a logger with a level of 'off', then no messages will be written out. A level of 'all' means that you will see all messages. Simple.<p class="paragraph"/><h4>Loggers</h4><p class="paragraph"/>Loggers are fundamental to the logging system, but they are a source of some confusion. For a start, what are they? Are they shared? How do you configure them?<p class="paragraph"/>A logger is the object you log messages to, so in the call <code>log.debug(msg)</code>, <code>log</code> is a logger instance (of type <a href="http://commons.apache.org/logging/apidocs/org/apache/commons/logging/Log.html" target="blank">Log</a>). These loggers are uniquely identified by name and if two separate classes use loggers with the same name, those loggers are effectively the same instance.<p class="paragraph"/>There are two main ways to get hold of a logger:
<ol>
<li>use the <code>log</code> instance injected into artifacts such as domain classes, controllers and services;</li>
<li>use the Commons Logging API directly.</li>
</ol><p class="paragraph"/>If you use the dynamic <code>log</code> property, then the name of the logger is 'grails.app.&#60;type&#62;.&#60;className&#62;', where <code>type</code> is the type of the artifact, say 'controller' or 'service, and <code>className</code> is the fully qualified name of the artifact. For example, let's say you have this service:<p class="paragraph"/><div class="code"><pre><span class="java&#45;keyword">package</span> org.example<p class="paragraph"/>class MyService &#123;
    &#8230;
&#125;</pre></div><p class="paragraph"/>then the name of the logger will be 'grails.app.service.org.example.MyService'.<p class="paragraph"/>For other classes, the typical approach is to store a logger based on the class name in a constant static field:<p class="paragraph"/><div class="code"><pre><span class="java&#45;keyword">package</span> org.other<p class="paragraph"/><span class="java&#45;keyword">import</span> org.apache.commons.logging.LogFactory<p class="paragraph"/>class MyClass &#123;
    <span class="java&#45;keyword">private</span> <span class="java&#45;keyword">static</span> <span class="java&#45;keyword">final</span> log = LogFactory.getLog(<span class="java&#45;keyword">this</span>)
    &#8230;
&#125;</pre></div><p class="paragraph"/>This will create a logger with the name 'org.other.MyClass' - note the lack of a 'grails.app.' prefix. You can also pass a name to the <code>getLog()</code> method, such as "myLogger", but this is less common because the logging system treats names with dots ('.') in a special way.<p class="paragraph"/><h4>Configuring loggers</h4><p class="paragraph"/>You have already seen how to configure a logger in Grails:<p class="paragraph"/><div class="code"><pre>log4j = &#123;
    error  'org.codehaus.groovy.grails.web.servlet'
&#125;</pre></div><p class="paragraph"/>This example configures a logger named 'org.codehaus.groovy.grails.web.servlet' to ignore any messages sent to it at a level of 'warn' or lower. But is there a logger with this name in the application? No. So why have a configuration for it? Because the above rule applies to any logger whose name  <em class="italic">begins with</em>  'org.codehaus.groovy.grails.servlet.' as well. For example, the rule applies to both the <code>org.codehaus.groovy.grails.web.servlet.GrailsDispatcherServlet</code> class and the <code>org.codehaus.groovy.grails.web.servlet.mvc.GrailsWebRequest</code> one.<p class="paragraph"/>In other words, loggers are effectively hierarchical. This makes configuring them by package much, much simpler than it would otherwise be.<p class="paragraph"/>The most common things that you will want to capture log output from are your controllers, services, and other artifacts. To do that you'll need to use the convention mentioned earlier:  <em class="italic">grails.app.&#60;artifactType&#62;.&#60;className&#62;</em> . In particular the class name must be fully qualifed, i.e. with the package if there is one:<p class="paragraph"/><div class="code"><pre>log4j = &#123;
    // Set level <span class="java&#45;keyword">for</span> all application artifacts
    info <span class="java&#45;quote">"grails.app"</span><p class="paragraph"/>    // Set <span class="java&#45;keyword">for</span> a specific controller
    debug <span class="java&#45;quote">"grails.app.controller.YourController"</span><p class="paragraph"/>    // Set <span class="java&#45;keyword">for</span> a specific domain class
    debug <span class="java&#45;quote">"grails.app.domain.org.example.Book"</span><p class="paragraph"/>    // Set <span class="java&#45;keyword">for</span> all taglibs
    info <span class="java&#45;quote">"grails.app.tagLib"</span>
&#125;</pre></div><p class="paragraph"/>The standard artifact names used in the logging configuration are:
<ul class="star">
<li><code>bootstrap</code> - For bootstrap classes</li>
<li><code>dataSource</code> - For data sources</li>
<li><code>tagLib</code> - For tag libraries</li>
<li><code>service</code> - For service classes</li>
<li><code>controller</code> - For controllers</li>
<li><code>domain</code> - For domain entities</li>
</ul><p class="paragraph"/>Grails itself generates plenty of logging information and it can sometimes be helpful to see that. Here are some useful loggers from Grails internals that you can use, especially when tracking down problems with your application:
<ul class="star">
<li><code>org.codehaus.groovy.grails.commons</code> - Core artifact information such as class loading etc.</li>
<li><code>org.codehaus.groovy.grails.web</code> - Grails web request processing</li>
<li><code>org.codehaus.groovy.grails.web.mapping</code> - URL mapping debugging</li>
<li><code>org.codehaus.groovy.grails.plugins</code> - Log plugin activity</li>
<li><code>grails.spring</code> - See what Spring beans Grails and plugins are defining</li>
<li><code>org.springframework</code> - See what Spring is doing</li>
<li><code>org.hibernate</code> - See what Hibernate is doing</li>
</ul><p class="paragraph"/>So far, we've only looked at explicit configuration of loggers. But what about all those loggers that  <em class="italic">don't</em>  have an explicit configuration? Are they simply ignored? The answer lies with the root logger.<p class="paragraph"/><h4>The Root Logger</h4><p class="paragraph"/>All logger objects inherit their configuration from the root logger, so if no explicit configuration is provided for a given logger, then any messages that go to that logger are subject to the rules defined for the root logger. In other words, the root logger provides the default configuration for the logging system.<p class="paragraph"/>Grails automatically configures the root logger to only handle messages at 'error' level and above, and all the messages are directed to the console (stdout for those with a C background). You can customise this behaviour by specifying a 'root' section in your logging configuration like so:<p class="paragraph"/><div class="code"><pre>log4j = &#123;
    root &#123;
        info()
    &#125;
    &#8230;
&#125;</pre></div><p class="paragraph"/>The above example configures the root logger to log messages at 'info' level and above to the default console appender. You can also configure the root logger to log to one or more named appenders (which we'll talk more about shortly):<p class="paragraph"/><div class="code"><pre>log4j = &#123;
    appenders &#123;
        file name:'file', file:'/<span class="java&#45;keyword">var</span>/logs/mylog.log'
    &#125;
    root &#123;
        debug 'stdout', 'file'
    &#125;
&#125;</pre></div><p class="paragraph"/>In the above example, the root logger will log to two appenders - the default 'stdout' (console) appender and a custom 'file' appender.<p class="paragraph"/>For power users there is an alternative syntax for configuring the root logger: the root <code>org.apache.log4j.Logger</code> instance is passed as an argument to the log4j closure. This allows you to work with the logger directly:<p class="paragraph"/><div class="code"><pre>log4j = &#123; root &#45;&#62;
    root.level = org.apache.log4j.Level.DEBUG
    &#8230;
&#125;</pre></div><p class="paragraph"/>For more information on what you can do with this <code>Logger</code> instance, refer to the Log4j API documentation.<p class="paragraph"/>Those are the basics of logging pretty well covered and they are sufficient if you're happy to only send log messages to the console. But what if you want to send them to a file? How do you make sure that messages from a particular logger go to a file but not the console? These questions and more will be answered as we look into appenders.<p class="paragraph"/><h3>Appenders</h3><p class="paragraph"/>Loggers are a useful mechanism for filtering messages, but they don't physically write the messages anywhere. That's the job of the appender, of which there are various types. For example, there is the default one that writes messages to the console, another that writes them to a file, and several others. You can even create your own appender implementations&#33;<p class="paragraph"/>This diagram shows how they fit into the logging pipeline:<p class="paragraph"/><img border="0" class="center" src="../img/logging.png"></img><p class="paragraph"/>As you can see, a single logger may have several appenders attached to it. In a standard Grails configuration, the console appender named 'stdout' is attached to all loggers through the default root logger configuration. But that's the only one. Adding more appenders can be done within an 'appenders' block:<p class="paragraph"/><div class="code"><pre>log4j = &#123;
    appenders &#123;
        rollingFile name: <span class="java&#45;quote">"myAppender"</span>, maxFileSize: 1024, file: <span class="java&#45;quote">"/tmp/logs/myApp.log"</span>
    &#125;
&#125;</pre></div><p class="paragraph"/>The following appenders are available by default:<p class="paragraph"/><table class="wiki-table" cellpadding="0" cellspacing="0" border="0"><tr><th><strong class="bold">Name</strong></th><th><strong class="bold">Class</strong></th><th><strong class="bold">Description</strong></th></tr><tr class="table-odd"><td>jdbc</td><td><a href="http://logging.apache.org/log4j/1.2/apidocs/org/apache/log4j/jdbc/JDBCAppender.html" target="blank">JDBCAppender</a></td><td>Logs to a JDBC connection.</td></tr><tr class="table-even"><td>console</td><td><a href="http://logging.apache.org/log4j/1.2/apidocs/org/apache/log4j/ConsoleAppender.html" target="blank">ConsoleAppender</a></td><td>Logs to the console.</td></tr><tr class="table-odd"><td>file</td><td><a href="http://logging.apache.org/log4j/1.2/apidocs/org/apache/log4j/FileAppender.html" target="blank">FileAppender</a></td><td>Logs to a single file.</td></tr><tr class="table-even"><td>rollingFile</td><td><a href="http://logging.apache.org/log4j/1.2/apidocs/org/apache/log4j/RollingFileAppender.html" target="blank">RollingFileAppender</a></td><td>Logs to rolling files, for example a new file each day.</td></tr></table><p class="paragraph"/>Each named argument passed to an appender maps to a property of the underlying <a href="http://logging.apache.org/log4j/1.2/apidocs/org/apache/log4j/Appender.html" target="blank">Appender</a> implementation. So the previous example sets the <code>name</code>, <code>maxFileSize</code> and <code>file</code> properties of the <code>RollingFileAppender</code> instance.<p class="paragraph"/>You can have as many appenders as you like - just make sure that they all have unique names. You can even have multiple instances of the same appender type, for example several file appenders that log to different files.<p class="paragraph"/>If you prefer to create the appender programmatically or if you want to use an appender implementation that's not available via the above syntax, then you can simply declare an <code>appender</code> entry with an instance of the appender you want:<p class="paragraph"/><div class="code"><pre><span class="java&#45;keyword">import</span> org.apache.log4j.&#42;<p class="paragraph"/>log4j = &#123;
    appenders &#123;
        appender <span class="java&#45;keyword">new</span> RollingFileAppender(name: <span class="java&#45;quote">"myAppender"</span>, maxFileSize: 1024, file: <span class="java&#45;quote">"/tmp/logs/myApp.log"</span>)
    &#125;
&#125;</pre></div><p class="paragraph"/>This approach can be used to configure <code>JMSAppender</code>, <code>SocketAppender</code>, <code>SMTPAppender</code>, and more.<p class="paragraph"/>Once you have declared your extra appenders, you can attach them to specific loggers by passing the name as a key to one of the log level methods from the previous section:<p class="paragraph"/><div class="code"><pre>error myAppender: <span class="java&#45;quote">"grails.app.controller.BookController"</span></pre></div><p class="paragraph"/>This will ensure that the 'org.codehaus.groovy.grails.commons' logger and its children send log messages to 'myAppender' as well as any appenders configured for the root logger. If you want to add more than one appender to the logger, then add them to the same level declaration:<p class="paragraph"/><div class="code"><pre>error myAppender:      <span class="java&#45;quote">"grails.app.controller.BookController"</span>,
      myFileAppender:  &#91;<span class="java&#45;quote">"grails.app.controller.BookController"</span>, <span class="java&#45;quote">"grails.app.service.BookService"</span>&#93;,
      rollingFile:     <span class="java&#45;quote">"grails.app.controller.BookController"</span></pre></div><p class="paragraph"/>The above example also shows how you can configure more than one logger at a time for a given appender (<code>myFileAppender</code>) by using a list.<p class="paragraph"/>Be aware that you can only configure a single level for a logger, so if you tried this code:<p class="paragraph"/><div class="code"><pre>error myAppender:      <span class="java&#45;quote">"grails.app.controller.BookController"</span>
debug myFileAppender:  <span class="java&#45;quote">"grails.app.controller.BookController"</span>
fatal rollingFile:     <span class="java&#45;quote">"grails.app.controller.BookController"</span></pre></div><p class="paragraph"/>you'd find that only 'fatal' level messages get logged for 'grails.app.controller.BookController'. That's because the last level declared for a given logger wins. What you probably want to do is limit what level of messages an appender writes.<p class="paragraph"/>Let's say an appender is attached to a logger configured with the 'all' level. That will give us a lot of logging information that may be fine in a file, but makes working at the console difficult. So, we configure the console appender to only write out messages at 'info' level or above:<p class="paragraph"/><div class="code"><pre>log4j = &#123;
    appenders &#123;
        console name: <span class="java&#45;quote">"stdout"</span>, threshold: org.apache.log4j.Level.INFO
    &#125;
&#125;</pre></div><p class="paragraph"/>The key here is the <code>threshold</code> argument which determines the cut-off for log messages. This argument is available for all appenders, but do note that you currently have to specify a <code>Level</code> instance - a string such as "info" will not work.<p class="paragraph"/><h3>Custom Layouts</h3><p class="paragraph"/>By default the Log4j DSL assumes that you want to use a <a href="http://logging.apache.org/log4j/1.2/apidocs/org/apache/log4j/PatternLayout.html" target="blank">PatternLayout</a>. However, there are other layouts available including:
<ul class="star">
<li><code>xml</code> - Create an XML log file</li>
<li><code>html</code> - Creates an HTML log file</li>
<li><code>simple</code> - A simple textual log</li>
<li><code>pattern</code> - A Pattern layout</li>
</ul><p class="paragraph"/>You can specify custom patterns to an appender using the <code>layout</code> setting:<p class="paragraph"/><div class="code"><pre>log4j = &#123;
    appenders &#123;
        console name: <span class="java&#45;quote">"customAppender"</span>, layout: pattern(conversionPattern: <span class="java&#45;quote">"%c&#123;2&#125; %m%n"</span>)
    &#125;
&#125;</pre></div><p class="paragraph"/>This also works for the built-in appender "stdout", which logs to the console:
<div class="code"><pre>log4j = &#123;
    appenders &#123;
        console name: <span class="java&#45;quote">"stdout"</span>, layout: pattern(conversionPattern: <span class="java&#45;quote">"%c&#123;2&#125; %m%n"</span>)
    &#125;
&#125;</pre></div><p class="paragraph"/><h3>Environment-specific configuration</h3><p class="paragraph"/>Since the logging configuration is inside <code>Config.groovy</code>, you can of course put it inside an environment-specific block. However, there is a problem with this approach: you have to provide the full logging configuration each time you define the <code>log4j</code> setting. In other words, you cannot selectively override parts of the configuration - it's all or nothing.<p class="paragraph"/>To get round this, the logging DSL provides its own environment blocks that you can put anywhere in the configuration:<p class="paragraph"/><div class="code"><pre>log4j = &#123;
    appenders &#123;
        console name: <span class="java&#45;quote">"stdout"</span>, layout: pattern(conversionPattern: <span class="java&#45;quote">"%c&#123;2&#125; %m%n"</span>)<p class="paragraph"/>        environments &#123;
            production &#123;
                rollingFile name: <span class="java&#45;quote">"myAppender"</span>, maxFileSize: 1024, file: <span class="java&#45;quote">"/tmp/logs/myApp.log"</span>
            &#125;
        &#125;
    &#125;<p class="paragraph"/>    root &#123;
        //&#8230;
    &#125;<p class="paragraph"/>    // other shared config
    info <span class="java&#45;quote">"grails.app.controller"</span><p class="paragraph"/>    environments &#123;
        production &#123;
            // Override previous setting <span class="java&#45;keyword">for</span> 'grails.app.controller'
            error <span class="java&#45;quote">"grails.app.controller"</span>
        &#125;
    &#125;
&#125;</pre></div><p class="paragraph"/>The one place you can't put an environment block is  <em class="italic">inside</em>  the <code>root</code> definition, but you can put the <code>root</code> definition inside an environment block.<p class="paragraph"/><h3>Full stacktraces</h3><p class="paragraph"/>When exceptions occur, there can be an awful lot of noise in the stacktrace from Java and Groovy internals. Grails filters these typically irrelevant details and restricts traces to non-core Grails/Groovy class packages.<p class="paragraph"/>When this happens, the full trace is always logged to the <code>StackTrace</code> logger, which by default writes its output to a file called <code>stacktrace.log</code>. As with other loggers though, you can change its behaviour in the configuration. For example if you prefer full stack traces to go to the console, add this entry:<p class="paragraph"/><div class="code"><pre>error stdout: <span class="java&#45;quote">"StackTrace"</span></pre></div><p class="paragraph"/>This won't stop Grails from attempting to create the stacktrace.log file - it just redirects where stack traces are written to. An alternative approach is to change the location of the 'stacktrace' appender's file:<p class="paragraph"/><div class="code"><pre>log4j = &#123;
    appenders &#123;
        rollingFile name: <span class="java&#45;quote">"stacktrace"</span>, maxFileSize: 1024, file: <span class="java&#45;quote">"/<span class="java&#45;keyword">var</span>/tmp/logs/myApp&#45;stacktrace.log"</span>
    &#125;
&#125;</pre></div><p class="paragraph"/>or, if you don't want to the 'stacktrace' appender at all, configure it as a 'null' appender:<p class="paragraph"/><div class="code"><pre>log4j = &#123;
    appenders &#123;
        '<span class="java&#45;keyword">null</span>' name: <span class="java&#45;quote">"stacktrace"</span>
    &#125;
&#125;</pre></div><p class="paragraph"/>You can of course combine this with attaching the 'stdout' appender to the 'StackTrace' logger if you want all the output in the console.<p class="paragraph"/>Finally, you can completely disable stacktrace filtering by setting the <code>grails.full.stacktrace</code> VM property to <code>true</code>:<p class="paragraph"/><div class="code"><pre>grails &#45;Dgrails.full.stacktrace=<span class="java&#45;keyword">true</span> run&#45;app</pre></div><p class="paragraph"/><h3>Masking Request Parameters From Stacktrace Logs</h3><p class="paragraph"/>When Grails logs a stacktrace, the log message may include the names and values of all of the request parameters for the current request.  To mask out the values of secure request parameters, specify the parameter names in the <code>grails.exceptionresolver.params.exclude</code> config property:<p class="paragraph"/><div class="code"><pre>grails.exceptionresolver.params.exclude = &#91;'password', 'creditCard'&#93;</pre></div><p class="paragraph"/>Request parameter logging may be turned off altogether by setting the <code>grails.exceptionresolver.logRequestParameters</code> config property to <code>false</code>.  The default value is <code>true</code> when the application is running in DEVELOPMENT mode and <code>false</code> for all other modes.<p class="paragraph"/><div class="code"><pre>grails.exceptionresolver.logRequestParameters=<span class="java&#45;keyword">false</span></pre></div><p class="paragraph"/><h3>Logger inheritance</h3><p class="paragraph"/>Earlier, we mentioned that all loggers inherit from the root logger and that loggers are hierarchical based on '.'-separated terms. What this means is that unless you override a parent setting, a logger retains the level and the appenders configured for that parent. So with this configuration:<p class="paragraph"/><div class="code"><pre>log4j = &#123;
    appenders &#123;
        file name:'file', file:'/<span class="java&#45;keyword">var</span>/logs/mylog.log'
    &#125;
    root &#123;
        debug 'stdout', 'file'
    &#125;
&#125;</pre></div><p class="paragraph"/>all loggers in the application will have a level of 'debug' and will log to both the 'stdout' and 'file' appenders. What if you only want to log to 'stdout' for a particular logger? In that case, you need to change the 'additivity' for a logger.<p class="paragraph"/>Additivity simply determines whether a logger inherits the configuration from its parent. If additivity is false, then its not inherited. The default for all loggers is true, i.e. they inherit the configuration. So how do you change this setting? Here's an example:<p class="paragraph"/><div class="code"><pre>log4j = &#123;
    appenders &#123;
        &#8230;
    &#125;
    root &#123;
        &#8230;
    &#125;<p class="paragraph"/>    info additivity: <span class="java&#45;keyword">false</span>
         stdout: &#91;<span class="java&#45;quote">"grails.app.controller.BookController"</span>, <span class="java&#45;quote">"grails.app.service.BookService"</span>&#93;
&#125;</pre></div><p class="paragraph"/>So when you specify a log level, add an 'additivity' named argument. Note that you when you specify the additivity, you must configure the loggers for a named appender. The following syntax will  <em class="italic">not</em>  work:<p class="paragraph"/><div class="code"><pre>info additivity: <span class="java&#45;keyword">false</span>, <span class="java&#45;quote">"grails.app.controller.BookController"</span>, <span class="java&#45;quote">"grails.app.service.BookService"</span></pre></div>

        </div>
    </body>
</html>
