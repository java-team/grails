<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <title>16. Scaffolding</title>
        <link rel="stylesheet" href="../../css/main.css" type="text/css" media="screen" title="Ref" charset="utf-8"/>
        <link rel="icon" href="../../img/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon"/>
    </head>
    <body class="body">
        <div id="header">
            <h1><a name="16. Scaffolding">16. Scaffolding</a></h1>
        </div>
        <div id="toc">
            
        </div>
        <div id="content">
            Scaffolding allows you to auto-generate a whole application for a given domain class including:
<ul class="star">
<li>The necessary <a href="../guide/single.html#6.2 Groovy Server Pages" class="guide">views</a></li>
<li>Controller actions for create/read/update/delete (CRUD) operations</li>
</ul><p class="paragraph"/><h4>Enabling Scaffolding</h4><p class="paragraph"/>The simplest way to get started with scaffolding is to enable scaffolding via the <code>scaffold</code> property. For the <code>Book</code> domain class, you need to set the <code>scaffold</code> property on a controller to true:<p class="paragraph"/><div class="code"><pre>class BookController &#123;
   <span class="java&#45;keyword">static</span> scaffold = <span class="java&#45;keyword">true</span>
&#125;</pre></div><p class="paragraph"/>The above works because the <code>BookController</code> follows the same naming convention as the <code>Book</code> domain class. If we wanted to scaffold a specific domain class we could reference the class directly in the scaffold property:<p class="paragraph"/><div class="code"><pre>class SomeController &#123;
    <span class="java&#45;keyword">static</span> scaffold = Author
&#125;</pre></div><p class="paragraph"/>With that done if you run this grails application the necessary actions and views will be auto-generated at runtime. The following actions are dynamically implemented by default by the runtime scaffolding mechanism:
<ul class="star">
<li>list</li>
<li>show</li>
<li>edit</li>
<li>delete</li>
<li>create</li>
<li>save</li>
<li>update</li>
</ul><p class="paragraph"/>As well as this a CRUD interface will be generated. To access the interface in the above example simply go to <code>http://localhost:8080/app/book</code><p class="paragraph"/>If you prefer to keep your domain model in Java and <a href="../guide/single.html#15. Grails and Hibernate" class="guide">mapped with Hibernate</a> you can still use scaffolding, simply import the necessary class and set the scaffold property to it.<p class="paragraph"/><h4>Dynamic Scaffolding</h4><p class="paragraph"/>Note that when using the scaffold property Grails does not use code templates, or code generation to achieve this so you can add your own actions to the scaffolded controller that interact with the scaffolded actions. For example, in the below example, <code>changeAuthor</code> redirects to the <code>show</code> action which doesn't actually exist physically:<p class="paragraph"/><div class="code"><pre>class BookController &#123;
    <span class="java&#45;keyword">static</span> scaffold = Book<p class="paragraph"/>    def changeAuthor = &#123;
        def b = Book.get( params&#91;<span class="java&#45;quote">"id"</span>&#93; )
        b.author = Author.get( params&#91;<span class="java&#45;quote">"author.id"</span>&#93; )
        b.save()<p class="paragraph"/>        // redirect to a scaffolded action
        redirect(action:show)
    &#125;
&#125;</pre></div><p class="paragraph"/>You can also override the scaffolded actions with your own actions if necessary:<p class="paragraph"/><div class="code"><pre>class BookController &#123;
    <span class="java&#45;keyword">static</span> scaffold = Book<p class="paragraph"/>    // overrides scaffolded action to <span class="java&#45;keyword">return</span> both authors and books
    def list = &#123;
        &#91; bookInstanceList : Book.list(), bookInstanceTotal: Book.count(), authorInstanceList: Author.list() &#93;
    &#125;<p class="paragraph"/>    def show = &#123;
        def book = Book.get(params.id)
        log.error(book)
        &#91; bookInstance : book &#93;
    &#125;<p class="paragraph"/>&#125;</pre></div><p class="paragraph"/>All of this is what is known as "dynamic scaffolding" where the CRUD interface is generated dynamically at runtime. Grails also supports "static" scaffolding which will be discussed in the following sections.<p class="paragraph"/><blockquote class="note">
By default, the size of text areas in scaffolded views is defined in the CSS, so adding 'rows' and 'cols' attributes will have no effect.<p class="paragraph"/>Also, the standard scaffold views expect model variables of the form <code>&#60;propertyName&#62;InstanceList</code> for collections and <code>&#60;propertyName&#62;Instance</code> for single instances. It's tempting to use properties like 'books' and 'book', but those won't work.
</blockquote><p class="paragraph"/><h4>Customizing the Generated Views</h4><p class="paragraph"/>The views that Grails generates have some form of intelligence in that they adapt to the <a href="../guide/single.html#7.1 Declaring Constraints" class="guide">Validation constraints</a>. For example you can change the order that fields appear in the views simply by re-ordering the constraints in the builder:<p class="paragraph"/><div class="code"><pre>def constraints = &#123;
   title()
   releaseDate()
&#125;</pre></div><p class="paragraph"/>You can also get the generator to generate lists instead of text inputs if you use the <code>inList</code> constraint:<p class="paragraph"/><div class="code"><pre>def constraints = &#123;
   title()
   category(inList:&#91;<span class="java&#45;quote">"Fiction"</span>, <span class="java&#45;quote">"Non&#45;fiction"</span>, <span class="java&#45;quote">"Biography"</span>&#93;)
   releaseDate()
&#125;</pre></div><p class="paragraph"/>Or if you use the <code>range</code> constraint on a number:<p class="paragraph"/><div class="code"><pre>def constraints = &#123;
   age(range:18..65)
&#125;</pre></div><p class="paragraph"/>Restricting the size via a constraint also effects how many characters can be entered in the generated view:<p class="paragraph"/><div class="code"><pre>def constraints = &#123;
   name(size:0..30)
&#125;</pre></div><p class="paragraph"/><h4>Generating Controllers &#38; Views</h4><p class="paragraph"/>The above scaffolding features are useful but in real world situations its likely that you will want to customize the logic and views. Grails allows you to generate a controller and the views used to create the above interface via the command line. To generate a controller type:<p class="paragraph"/><div class="code"><pre>grails generate&#45;controller Book</pre></div><p class="paragraph"/>Or to generate the views type:<p class="paragraph"/><div class="code"><pre>grails generate&#45;views Book</pre></div><p class="paragraph"/>Or to generate everything type:<p class="paragraph"/><div class="code"><pre>grails generate&#45;all Book</pre></div><p class="paragraph"/>If you have a domain class in a package or are generating from a <a href="../guide/single.html#15. Grails and Hibernate" class="guide">Hibernate mapped class</a> remember to include the fully qualified package name:<p class="paragraph"/><div class="code"><pre>grails generate&#45;all com.bookstore.Book</pre></div><p class="paragraph"/><h4>Customizing the Scaffolding templates</h4><p class="paragraph"/>The templates used by Grails to generate the controller and views can be customized by installing the templates with the <a href="../ref/Command Line/install-templates.html" class="commandLine">install-templates</a> command.

        </div>
    </body>
</html>
