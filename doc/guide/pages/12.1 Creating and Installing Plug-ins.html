<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <title>12.1 Creating and Installing Plug-ins</title>
        <link rel="stylesheet" href="../../css/main.css" type="text/css" media="screen" title="Ref" charset="utf-8"/>
        <link rel="icon" href="../../img/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon"/>
    </head>
    <body class="body">
        <div id="header">
            <h2><a name="12.1 Creating and Installing Plug-ins">12.1 Creating and Installing Plug-ins</a></h2>
        </div>
        <div id="toc">
            
        </div>
        <div id="content">
            <h4>Creating Plugins</h4><p class="paragraph"/>Creating a Grails plugin is a simple matter of running the command:<p class="paragraph"/><div class="code"><pre>grails create&#45;plugin &#91;PLUGIN NAME&#93;</pre></div><p class="paragraph"/>This will create a plugin project for the name you specify. Say for example you run <code>grails create-plugin example</code>. This would create a new plugin project called <code>example</code>.<p class="paragraph"/>The structure of a Grails plugin is exactly the same as a regular Grails project's directory structure, except that in the root of the plugin directory you will find a plugin Groovy file called the "plugin descriptor".<p class="paragraph"/>Being a regular Grails project has a number of benefits in that you can immediately get going testing your plugin by running:<p class="paragraph"/><div class="code"><pre>grails run&#45;app</pre></div><p class="paragraph"/>The plugin descriptor itself ends with the convention <code>GrailsPlugin</code> and is found in the root of the plugin project. For example:<p class="paragraph"/><div class="code"><pre>class ExampleGrailsPlugin &#123;
   def version = 0.1<p class="paragraph"/>   &#8230;
&#125;</pre></div><p class="paragraph"/>All plugins must have this class in the root of their directory structure to be valid. The plugin class defines the version of the plugin and optionally various hooks into plugin extension points (covered shortly).<p class="paragraph"/>You can also provide additional information about your plugin using several special properties:
<ul class="star">
<li><code>title</code> - short one sentence description of your plugin</li>
<li><code>version</code> - The version of your problem. Valid versions are for example "0.1", "0.2-SNAPSHOT", "0.1.4" etc.</li>
<li><code>grailsVersion</code> - The version of version range of Grails that the plugin supports. eg. "1.1 &#62; *"</li>
<li><code>author</code> - plug-in author's name</li>
<li><code>authorEmail</code> - plug-in author's contact e-mail</li>
<li><code>description</code> - full multi-line description of plug-in's features</li>
<li><code>documentation</code> - URL where plug-in's documentation can be found</li>
</ul><p class="paragraph"/>Here is an example from <a href="http://grails.org/Quartz+plugin:" target="blank">Quartz Grails plugin</a><p class="paragraph"/><div class="code"><pre>class QuartzGrailsPlugin &#123;
    def version = <span class="java&#45;quote">"0.1"</span>
	def grailsVersion = <span class="java&#45;quote">"1.1 &#62; &#42;"</span>
    def author = <span class="java&#45;quote">"Sergey Nebolsin"</span>
    def authorEmail = <span class="java&#45;quote">"nebolsin@gmail.com"</span>
    def title = <span class="java&#45;quote">"This plugin adds Quartz job scheduling features to Grails application."</span>
    def description = '''
Quartz plugin allows your Grails application to schedule jobs to be
executed using a specified interval or cron expression. The underlying
system uses the Quartz Enterprise Job Scheduler configured via Spring,
but is made simpler by the coding by convention paradigm.
'''
    def documentation = <span class="java&#45;quote">"http://grails.org/Quartz+plugin"</span><p class="paragraph"/>   &#8230;
&#125;</pre></div><p class="paragraph"/><h4>Installing &#38; Distributing Plugins</h4><p class="paragraph"/>To distribute a plugin you need to navigate to its root directory in a terminal window and then type:<p class="paragraph"/><div class="code"><pre>grails <span class="java&#45;keyword">package</span>&#45;plugin</pre></div><p class="paragraph"/>This will create a zip file of the plugin starting with <code>grails-</code> then the plugin name and version. For example with the example plug-in created earlier this would be <code>grails-example-0.1.zip</code>. The <code>package-plugin</code> command will also generate <code>plugin.xml</code> file which contains machine-readable information about plugin's name, version, author, and so on.<p class="paragraph"/>Once you have a plugin distribution file you can navigate to a Grails project and type:<p class="paragraph"/><div class="code"><pre>grails install&#45;plugin /path/to/plugin/grails&#45;example&#45;0.1.zip</pre></div><p class="paragraph"/>If the plugin is hosted on a remote HTTP server you can also do:<p class="paragraph"/><div class="code"><pre>grails install&#45;plugin http://myserver.com/plugins/grails&#45;example&#45;0.1.zip</pre></div><p class="paragraph"/><h4>Notes on excluded Artefacts</h4><p class="paragraph"/>Although the <a href="../ref/Command Line/create-plugin.html" class="commandLine">create-plugin</a> command creates certain files for you so that the plug-in can be run as a Grails application, not all of these files are included when packaging a plug-in. The following is a list of artefacts created, but not included by <a href="../ref/Command Line/package-plugin.html" class="commandLine">package-plugin</a>:
<ul class="star">
<li><code>grails-app/conf/DataSource.groovy</code></li>
<li><code>grails-app/conf/UrlMappings.groovy</code></li>
<li><code>build.xml</code></li>
<li>Everything within <code>/web-app/WEB-INF</code></li>
</ul><p class="paragraph"/>If you need artefacts within <code>WEB-INF</code> it is recommended you use the <code>_Install.groovy</code> script (covered later), which is executed when a plug-in is installed, to provide such artefacts. In addition, although <code>UrlMappings.groovy</code> is excluded you are allowed to include a <code>UrlMappings</code> definition with a different name, such as <code>FooUrlMappings.groovy</code>.<p class="paragraph"/><h4>Specifying Plugin Locations</h4><p class="paragraph"/>An application can load plugins from anywhere on the file system, even if they have not been installed. Simply add the location of the (unpacked) plugin to the application's <code>grails-app/conf/BuildConfig.groovy</code> file:<p class="paragraph"/><div class="code"><pre>// Useful to test plugins you are developing.
grails.plugin.location.jsecurity = <span class="java&#45;quote">"/home/dilbert/dev/plugins/grails&#45;jsecurity"</span><p class="paragraph"/>// Useful <span class="java&#45;keyword">for</span> modular applications where all plugins and
// applications are in the same directory.
grails.plugin.location.'grails&#45;ui' = <span class="java&#45;quote">"../grails&#45;grails&#45;ui"</span></pre></div><p class="paragraph"/>This is particularly useful in two cases:
<ul class="star">
<li>You are developing a plugin and want to test it in a real application without packaging and installing it first.</li>
<li>You have split an application into a set of plugins and an application, all in the same "super-project" directory.</li>
</ul><p class="paragraph"/>	

        </div>
    </body>
</html>
