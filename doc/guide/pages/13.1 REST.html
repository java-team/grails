<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <title>13.1 REST</title>
        <link rel="stylesheet" href="../../css/main.css" type="text/css" media="screen" title="Ref" charset="utf-8"/>
        <link rel="icon" href="../../img/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon"/>
    </head>
    <body class="body">
        <div id="header">
            <h2><a name="13.1 REST">13.1 REST</a></h2>
        </div>
        <div id="toc">
            
        </div>
        <div id="content">
            REST is not really a technology in itself, but more an architectural pattern. REST is extremely simple and just involves using plain XML or JSON as a communication medium, combined with URL patterns that are "representational" of the underlying system and HTTP methods such as GET, PUT, POST and DELETE.<p class="paragraph"/>Each HTTP method maps to an action. For example GET for retrieving data, PUT for creating data, POST for updating and so on. In this sense REST fits quite well with <a href="../guide/single.html#16. Scaffolding" class="guide">CRUD</a>.<p class="paragraph"/><h4>URL patterns</h4><p class="paragraph"/>The first step to implementing REST with Grails is to provide RESTful <a href="../guide/single.html#6.4 URL Mappings" class="guide">URL mappings</a>:<p class="paragraph"/><div class="code"><pre><span class="java&#45;keyword">static</span> mappings = &#123;
   <span class="java&#45;quote">"/product/$id?"</span>(resource:<span class="java&#45;quote">"product"</span>)
&#125;</pre></div><p class="paragraph"/>What this does is map the URI <code>/product</code> onto a <code>ProductController</code>. Each HTTP method such as GET, PUT, POST and DELETE map to unique actions within the controller as outlined by the table below:<p class="paragraph"/><table class="wiki-table" cellpadding="0" cellspacing="0" border="0"><tr><th>Method</th><th>Action</th></tr><tr class="table-odd"><td><code>GET</code></td><td><code>show</code></td></tr><tr class="table-even"><td><code>PUT</code></td><td><code>update</code></td></tr><tr class="table-odd"><td><code>POST</code></td><td><code>save</code></td></tr><tr class="table-even"><td><code>DELETE</code></td><td><code>delete</code></td></tr></table><p class="paragraph"/>You can alter how HTTP methods by using the capability of URL Mappings to <a href="../guide/single.html#6.4.5 Mapping to HTTP methods" class="guide">map to HTTP methods</a>:<p class="paragraph"/><div class="code"><pre><span class="java&#45;quote">"/product/$id"</span>(controller:<span class="java&#45;quote">"product"</span>)&#123;
    action = &#91;GET:<span class="java&#45;quote">"show"</span>, PUT:<span class="java&#45;quote">"update"</span>, DELETE:<span class="java&#45;quote">"delete"</span>, POST:<span class="java&#45;quote">"save"</span>&#93;
&#125;</pre></div><p class="paragraph"/>However, unlike the <code>resource</code> argument used previously, in this case Grails will not provide automatic XML or JSON marshaling for you unless you specify the <code>parseRequest</code> argument in the URL mapping:<p class="paragraph"/><div class="code"><pre><span class="java&#45;quote">"/product/$id"</span>(controller:<span class="java&#45;quote">"product"</span>, parseRequest:<span class="java&#45;keyword">true</span>)&#123;
    action = &#91;GET:<span class="java&#45;quote">"show"</span>, PUT:<span class="java&#45;quote">"update"</span>, DELETE:<span class="java&#45;quote">"delete"</span>, POST:<span class="java&#45;quote">"save"</span>&#93;
&#125;</pre></div><p class="paragraph"/><h4>HTTP Methods</h4><p class="paragraph"/>In the previous section you saw how you can easily define URL mappings that map specific HTTP methods onto specific controller actions. Writing a REST client that then sends a specific HTTP method is then trivial (example in Groovy's HTTPBuilder module):<p class="paragraph"/><div class="code"><pre><span class="java&#45;keyword">import</span> groovyx.net.http.&#42;
<span class="java&#45;keyword">import</span> <span class="java&#45;keyword">static</span> groovyx.net.http.ContentType.JSON<p class="paragraph"/>def http = <span class="java&#45;keyword">new</span> HTTPBuilder(<span class="java&#45;quote">"http://localhost:8080/amazon"</span>)<p class="paragraph"/> http.request(Method.GET, JSON) &#123;
     url.path = '/book/list'
     response.success = &#123;resp, json &#45;&#62;
         json.books.each &#123; book &#45;&#62;
             println book.title
         &#125;
     &#125;
 &#125;</pre></div><p class="paragraph"/>However, issuing a request with a method other than <code>GET</code> or <code>POST</code> from a regular browser is not possible without some help from Grails. When defining a <a href="../ref/Tags/form.html" class="tags">form</a> you can specify an alternative method such as <code>DELETE</code>:<p class="paragraph"/><div class="code"><pre>&#60;g:form controller=<span class="java&#45;quote">"book"</span> method=<span class="java&#45;quote">"DELETE"</span>&#62;
	..	
&#60;/g:form&#62;</pre></div><p class="paragraph"/>Grails will send a hidden parameter called <code>_method</code>, which will be used as the request's HTTP method. Another alternative for changing the method for non-browser clients is to use the <code>X-HTTP-Method-Override</code> to specify the alternative method name.<p class="paragraph"/><h4>XML Marshaling - Reading</h4><p class="paragraph"/>The controller implementation itself can use Grails' <a href="../guide/single.html#6.1.7 XML and JSON Responses" class="guide">XML marshaling</a> support to implement the GET method:<p class="paragraph"/><div class="code"><pre><span class="java&#45;keyword">import</span> grails.converters.&#42;
class ProductController &#123;
	def show = &#123;
		<span class="java&#45;keyword">if</span>(params.id &#38;&#38; Product.exists(params.id)) &#123;
			def p = Product.findByName(params.id)
			render p as XML
		&#125;
		<span class="java&#45;keyword">else</span> &#123;
			def all = Product.list()
			render all as XML
		&#125;
	&#125;
	..
&#125;</pre></div><p class="paragraph"/>Here what we do is if there is an <code>id</code> we search for the <code>Product</code> by name and return it otherwise we return all Products. This way if we go to <code>/products</code> we get all products, otherwise if we go to <code>/product/MacBook</code> we only get a MacBook.<p class="paragraph"/><h4>XML Marshalling - Updating</h4><p class="paragraph"/>To support updates such as <code>PUT</code> and <code>POST</code> you can use the <a href="../ref/Controllers/params.html" class="controllers">params</a> object which Grails enhances with the ability to read an incoming XML packet. Given an incoming XML packet of:<p class="paragraph"/><div class="code"><pre><span class="xml&#45;tag">&#60;?xml version=<span class="xml&#45;quote">"1.0"</span> encoding=<span class="xml&#45;quote">"ISO&#45;8859&#45;1"</span>?&#62;</span>
<span class="xml&#45;tag">&#60;product&#62;</span>
	<span class="xml&#45;tag">&#60;name&#62;</span>MacBook<span class="xml&#45;tag">&#60;/name&#62;</span>
	<span class="xml&#45;tag">&#60;vendor id=<span class="xml&#45;quote">"12"</span>&#62;</span>
		<span class="xml&#45;tag">&#60;name&#62;</span>Apple<span class="xml&#45;tag">&#60;/name&#62;</span>
     <span class="xml&#45;tag">&#60;/vender&#62;</span>
<span class="xml&#45;tag">&#60;/product&#62;</span></pre></div><p class="paragraph"/>You can read this XML packet using the same techniques described in the <a href="../guide/single.html#6.1.6 Data Binding" class="guide">Data Binding</a> section via the <a href="../ref/Controllers/params.html" class="controllers">params</a> object:<p class="paragraph"/><div class="code"><pre>def save = &#123;
	def p = <span class="java&#45;keyword">new</span> Product(params&#91;'product'&#93;)<p class="paragraph"/>	<span class="java&#45;keyword">if</span>(p.save()) &#123;
		render p as XML
	&#125;
	<span class="java&#45;keyword">else</span> &#123;
		render p.errors
	&#125;
&#125;</pre></div><p class="paragraph"/>In this example by indexing into the <code>params</code> object using the key <code>'product'</code> we can automatically create and bind the XML using the constructor of the <code>Product</code> class. An interesting aspect of the line:
<div class="code"><pre>def p = <span class="java&#45;keyword">new</span> Product(params&#91;'product'&#93;)</pre></div>
Is that it requires no code changes to deal with a form submission that submits form data than it does to deal with an XML request. The exact same technique can be used with a JSON request too.<p class="paragraph"/><blockquote class="note">
If you require different responses to different clients (REST, HTML etc.) you can use <a href="../guide/single.html#6.8 Content Negotiation" class="guide">content negotation</a>
</blockquote><p class="paragraph"/>The <code>Product</code> object is then saved and rendered as XML, otherwise an error message is produced using Grails' <a href="../guide/single.html#7. Validation" class="guide">validation</a> capabilities in the form:<p class="paragraph"/><div class="code"><pre><span class="xml&#45;tag">&#60;error&#62;</span>
   <span class="xml&#45;tag">&#60;message&#62;</span>The property 'title' of class 'Person' must be specified<span class="xml&#45;tag">&#60;/message&#62;</span>
<span class="xml&#45;tag">&#60;/error&#62;</span></pre></div> 
        </div>
    </body>
</html>
