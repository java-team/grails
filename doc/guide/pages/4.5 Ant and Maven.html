<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <title>4.5 Ant and Maven</title>
        <link rel="stylesheet" href="../../css/main.css" type="text/css" media="screen" title="Ref" charset="utf-8"/>
        <link rel="icon" href="../../img/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon"/>
    </head>
    <body class="body">
        <div id="header">
            <h2><a name="4.5 Ant and Maven">4.5 Ant and Maven</a></h2>
        </div>
        <div id="toc">
            
        </div>
        <div id="content">
            If all the other projects in your team or company are built using a standard build tool such as Ant or Maven, you become the black sheep of the family when you use the Grails command line to build your application. Fortunately, you can easily integrate the Grails build system into the main build tools in use today (well, the ones in use in Java projects at least).<p class="paragraph"/><h3>Ant Integration</h3><p class="paragraph"/>When you create a Grails application via the <a href="../ref/Command Line/create-app.html" class="commandLine">create-app</a> command, Grails automatically creates an <a href="http://ant.apache.org/" target="blank">Apache Ant</a> <code>build.xml</code> file for you containing the following targets:
<ul class="star">
<li><code>clean</code> - Cleans the Grails application</li>
<li><code>compile</code> - Compiles your application's source code</li>
<li><code>test</code> - Runs the unit tests</li>
<li><code>run</code> - Equivalent to "grails run-app"</li>
<li><code>war</code> - Creates a WAR file</li>
<li><code>deploy</code> - Empty by default, but can be used to implement automatic deployment</li>
</ul><p class="paragraph"/>Each of these can be run by Ant, for example:<p class="paragraph"/><div class="code"><pre>ant war</pre></div><p class="paragraph"/>The build file is all geared up to use <a href="http://ant.apache.org/ivy/" target="blank">Apache Ivy</a> for dependency management, which means that it will automatically download all the requisite Grails JAR files and other dependencies on demand. You don't even have to install Grails locally to use it! That makes it particularly useful for continuous integration systems such as <a href="http://cruisecontrol.sourceforge.net/" target="blank">CruiseControl</a> or <a href="https://hudson.dev.java.net/." target="blank">Hudson</a><p class="paragraph"/>It uses the Grails <a href="../api/grails/ant/GrailsTask.html" class="api">Ant task</a> to hook into the existing Grails build system. The task allows you to run any Grails script that's available, not just the ones used by the generated build file. To use the task, you must first declare it:
<div class="code"><pre>&#60;taskdef name=<span class="java&#45;quote">"grailsTask"</span>
         classname=<span class="java&#45;quote">"grails.ant.GrailsTask"</span>
         classpathref=<span class="java&#45;quote">"grails.classpath"</span>/&#62;</pre></div><p class="paragraph"/>This raises the question: what should be in "grails.classpath"? The task itself is in the "grails-bootstrap" JAR artifact, so that needs to be on the classpath at least. You should also include the "groovy-all" JAR. With the task defined, you just need to use it! The following table shows you what attributes are available:
<table class="wiki-table" cellpadding="0" cellspacing="0" border="0"><tr><th>Attribute</th><th>Description</th><th>Required</th></tr><tr class="table-odd"><td>home</td><td>The location of the Grails installation directory to use for the build.</td><td>Yes, unless classpath is specified.</td></tr><tr class="table-even"><td>classpathref</td><td>Classpath to load Grails from. Must include the "grails-bootstrap" artifact and should include "grails-scripts".</td><td>Yes, unless <code>home</code> is set or you use a <code>classpath</code> element.</td></tr><tr class="table-odd"><td>script</td><td>The name of the Grails script to run, e.g. "TestApp".</td><td>Yes.</td></tr><tr class="table-even"><td>args</td><td>The arguments to pass to the script, e.g. "-unit -xml".</td><td>No. Defaults to "".</td></tr><tr class="table-odd"><td>environment</td><td>The Grails environment to run the script in.</td><td>No. Defaults to the script default.</td></tr><tr class="table-even"><td>includeRuntimeClasspath</td><td>Advanced setting: adds the application's runtime classpath to the build classpath if true.</td><td>No. Defaults to true.</td></tr></table><p class="paragraph"/>The task also supports the following nested elements, all of which are standard Ant path structures:
<ul class="star">
<li><code>classpath</code> - The build classpath (used to load Gant and the Grails scripts).</li>
<li><code>compileClasspath</code> - Classpath used to compile the application's classes.</li>
<li><code>runtimeClasspath</code> - Classpath used to run the application and package the WAR. Typically includes everything in @compileClasspath.</li>
<li><code>testClasspath</code> - Classpath used to compile and run the tests. Typically includes everything in <code>runtimeClasspath</code>.</li>
</ul><p class="paragraph"/>How you populate these paths is up to you. If you are using the <code>home</code> attribute and put your own dependencies in the <code>lib</code> directory, then you don't even need to use any of them. For an example of their use, take a look at the generated Ant build file for new apps.<p class="paragraph"/><h3>Maven Integration</h3><p class="paragraph"/>From 1.1 onwards, Grails provides integration with <a href="http://maven.apache.org" target="blank">Maven 2</a> via a Maven plugin. The current Maven plugin is based on, but effectively supersedes, the version created by <a href="http://forge.octo.com/" target="blank">Octo</a>, who did a great job.<p class="paragraph"/><h4>Preparation</h4><p class="paragraph"/>In order to use the new plugin, all you need is Maven 2 installed and set up. This is because <strong class="bold">you no longer need to install Grails separately to use it with Maven!</strong><p class="paragraph"/><blockquote class="note">
The Maven 2 integration for Grails has been designed and tested for Maven 2.0.9 and above. It will not work with earlier versions.
</blockquote><p class="paragraph"/><blockquote class="note">
The default mvn setup DOES NOT supply sufficient memory to run the Grails environment. We recommend that you add the following environment variable setting to prevent poor performance:<p class="paragraph"/><code>export MAVEN_OPTS="-Xmx512m -XX:MaxPermSize=192m"</code>
</blockquote><p class="paragraph"/><h4>Creating a Grails Maven Project</h4><p class="paragraph"/>To create a Mavenized Grails project simply run the following command:<p class="paragraph"/><div class="code"><pre>mvn archetype:generate &#45;DarchetypeGroupId=org.grails &#92;
    &#45;DarchetypeArtifactId=grails&#45;maven&#45;archetype &#92;
    &#45;DarchetypeVersion=1.3.2 &#92;
    &#45;DgroupId=example &#45;DartifactId=my&#45;app</pre></div><p class="paragraph"/>Choose whichever grails version, group ID and artifact ID you want for your application, but everything else must be as written. This will create a new Maven project with a POM and a couple of other files. What you won't see is anything that looks like a Grails application. So, the next step is to create the project structure that you're used to.
But first, if you want to set target JDK to Java 6, do that now. Open my-app/pom.xml and change
<div class="code"><pre>&#60;plugin&#62;
  &#60;artifactId&#62;maven&#45;compiler&#45;plugin&#60;/artifactId&#62;
  &#60;configuration&#62;
    &#60;source&#62;1.5&#60;/source&#62;
    &#60;target&#62;1.5&#60;/target&#62;
  &#60;/configuration&#62;
&#60;/plugin&#62;</pre></div>
to
<div class="code"><pre>&#60;plugin&#62;
  &#60;artifactId&#62;maven&#45;compiler&#45;plugin&#60;/artifactId&#62;
  &#60;configuration&#62;
    &#60;source&#62;1.6&#60;/source&#62;
    &#60;target&#62;1.6&#60;/target&#62;
  &#60;/configuration&#62;
&#60;/plugin&#62;</pre></div><p class="paragraph"/>Then you're ready to create the project structure:<p class="paragraph"/><div class="code"><pre>cd my&#45;app
mvn initialize</pre></div><p class="paragraph"/><blockquote class="note">
if you see a message similar to this:<p class="paragraph"/><div class="code"><pre>Resolving plugin JAR dependencies &#8230;
:: problems summary ::
:::: WARNINGS
		module not found: org.hibernate&#35;hibernate&#45;core;3.3.1.GA</pre></div><p class="paragraph"/>you need to add the plugins manually to application.properties:<p class="paragraph"/><div class="code"><pre>plugins.hibernate=1.3.2
plugins.tomcat=1.3.2</pre></div><p class="paragraph"/>then run<p class="paragraph"/><div class="code"><pre>mvn compile</pre></div><p class="paragraph"/>and the hibernate and tomcat plugins will be installed.
</blockquote><p class="paragraph"/>Now you have a Grails application all ready to go. The plugin integrates into the standard build cycle, so you can use the standard Maven phases to build and package your app:  <code>mvn clean</code> ,  <code>mvn compile</code> ,  <code>mvn test</code> ,  <code>mvn package</code> , <code>mvn install</code> .<p class="paragraph"/>You can also take advantage of some of the Grails commands that have been wrapped as Maven goals:
<ul class="star">
<li><code>grails:create-controller</code> - Calls the <a href="../ref/Command Line/create-controller.html" class="commandLine">create-controller</a> command</li>
<li><code>grails:create-domain-class</code> - Calls the <a href="../ref/Command Line/create-domain-class.html" class="commandLine">create-domain-class</a> command</li>
<li><code>grails:create-integration-test</code> - Calls the <a href="../ref/Command Line/create-integration-test.html" class="commandLine">create-integration-test</a> command</li>
<li><code>grails:create-pom</code> - Creates a new Maven POM for an existing Grails project</li>
<li><code>grails:create-script</code> - Calls the <a href="../ref/Command Line/create-script.html" class="commandLine">create-script</a> command</li>
<li><code>grails:create-service</code> - Calls the <a href="../ref/Command Line/create-service.html" class="commandLine">create-service</a> command</li>
<li><code>grails:create-taglib</code> - Calls the <a href="../ref/Command Line/create-tag-lib.html" class="commandLine">create-tag-lib</a> command</li>
<li><code>grails:create-unit-test</code> - Calls the <a href="../ref/Command Line/create-unit-test.html" class="commandLine">create-unit-test</a> command</li>
<li><code>grails:exec</code> - Executes an arbitrary Grails command line script</li>
<li><code>grails:generate-all</code> - Calls the <a href="../ref/Command Line/generate-all.html" class="commandLine">generate-all</a> command</li>
<li><code>grails:generate-controller</code>  - Calls the <a href="../ref/Command Line/generate-controller.html" class="commandLine">generate-controller</a> command</li>
<li><code>grails:generate-views</code> - Calls the <a href="../ref/Command Line/generate-views.html" class="commandLine">generate-views</a> command</li>
<li><code>grails:install-plugin</code> - Calls the <a href="../ref/Command Line/install-plugin.html" class="commandLine">install-plugin</a> command</li>
<li><code>grails:install-templates</code> - Calls the <a href="../ref/Command Line/install-templates.html" class="commandLine">install-templates</a> command</li>
<li><code>grails:list-plugins</code> - Calls the <a href="../ref/Command Line/list-plugins.html" class="commandLine">list-plugins</a> command</li>
<li><code>grails:package</code> - Calls the <a href="../ref/Command Line/package.html" class="commandLine">package</a> command</li>
<li><code>grails:run-app</code> - Calls the <a href="../ref/Command Line/run-app.html" class="commandLine">run-app</a> command</li>
<li><code>grails:uninstall-plugin</code> - Calls the <a href="../ref/Command Line/uninstall-plugin.html" class="commandLine">uninstall-plugin</a> command</li>
</ul><p class="paragraph"/>For a complete, up to date list, run <code>mvn grails:help</code><p class="paragraph"/><h4>Mavenizing an existing project</h4><p class="paragraph"/>Creating a new project is great way to start, but what if you already have one? You don't want to create a new project and then copy the contents of the old one over. The solution is to create a POM for the existing project using this Maven command (substitute the version number with the grails version of your existing project):
<div class="code"><pre>mvn org.grails:grails&#45;maven&#45;plugin:1.3.2:create&#45;pom &#45;DgroupId=com.mycompany</pre></div>
When this command has finished, you can immediately start using the standard phases, such as <code>mvn package</code>. Note that you have to specify a group ID when creating the POM.<p class="paragraph"/>You may also want to set target JDK to Java 6, see above.<p class="paragraph"/><h4>Adding Grails commands to phases</h4><p class="paragraph"/>The standard POM created for you by Grails already attaches the appropriate core Grails commands to their corresponding build phases, so "compile" goes in the "compile" phase and "war" goes in the "package" phase. That doesn't help though when you want to attach a plugin's command to a particular phase. The classic example is functional tests. How do you make sure that your functional tests (using which ever plugin you have decided on) are run during the "integration-test" phase?<p class="paragraph"/>Fear not: all things are possible. In this case, you can associate the command to a phase using an extra "execution" block:
<div class="code"><pre><span class="xml&#45;tag">&#60;plugin&#62;</span>
    <span class="xml&#45;tag">&#60;groupId&#62;</span>org.grails<span class="xml&#45;tag">&#60;/groupId&#62;</span>
    <span class="xml&#45;tag">&#60;artifactId&#62;</span>grails&#45;maven&#45;plugin<span class="xml&#45;tag">&#60;/artifactId&#62;</span>
    <span class="xml&#45;tag">&#60;version&#62;</span>1.3.2<span class="xml&#45;tag">&#60;/version&#62;</span>
    <span class="xml&#45;tag">&#60;extensions&#62;</span>true<span class="xml&#45;tag">&#60;/extensions&#62;</span>
    <span class="xml&#45;tag">&#60;executions&#62;</span>
        <span class="xml&#45;tag">&#60;execution&#62;</span>
            <span class="xml&#45;tag">&#60;goals&#62;</span>
            &#8230;
            <span class="xml&#45;tag">&#60;/goals&#62;</span>
        <span class="xml&#45;tag">&#60;/execution&#62;</span>
        <span class="xml&#45;comment">&#60;!&#45;&#45; Add the <span class="xml&#45;quote">"functional&#45;tests"</span> command to the <span class="xml&#45;quote">"integration&#45;test"</span> phase &#45;&#45;&#62;</span>
        <span class="xml&#45;tag">&#60;execution&#62;</span>
            <span class="xml&#45;tag">&#60;id&#62;</span>functional&#45;tests<span class="xml&#45;tag">&#60;/id&#62;</span>
            <span class="xml&#45;tag">&#60;phase&#62;</span>integration&#45;test<span class="xml&#45;tag">&#60;/phase&#62;</span>
            <span class="xml&#45;tag">&#60;goals&#62;</span>
                <span class="xml&#45;tag">&#60;goal&#62;</span>exec<span class="xml&#45;tag">&#60;/goal&#62;</span>
            <span class="xml&#45;tag">&#60;/goals&#62;</span>
            <span class="xml&#45;tag">&#60;configuration&#62;</span>
                <span class="xml&#45;tag">&#60;command&#62;</span>functional&#45;tests<span class="xml&#45;tag">&#60;/command&#62;</span>
            <span class="xml&#45;tag">&#60;/configuration&#62;</span>
        <span class="xml&#45;tag">&#60;/execution&#62;</span>
    <span class="xml&#45;tag">&#60;/executions&#62;</span>
<span class="xml&#45;tag">&#60;/plugin&#62;</span></pre></div><p class="paragraph"/>This also demonstrates the <code>grails:exec</code> goal, which can be used to run any Grails command. Simply pass the name of the command as the <code>command</code> system property, and optionally specify the arguments via the <code>args</code> property:
<div class="code"><pre>mvn grails:exec &#45;Dcommand=create&#45;webtest &#45;Dargs=Book</pre></div><p class="paragraph"/><h4>Debugging a Grails Maven Project</h4><p class="paragraph"/>Maven can be launched in debug mode using the "mvnDebug" command. To launch your Grails application in debug, simply run:<p class="paragraph"/><div class="code"><pre>mvnDebug grails:run&#45;app</pre></div><p class="paragraph"/>The process will be suspended on startup and listening for a debugger on port 8000.<p class="paragraph"/>If you need more control of the debugger, this can be specified using the MAVEN_OPTS environment variable, and launch Maven via the default "mvn" command:<p class="paragraph"/><div class="code"><pre>MAVEN_OPTS=<span class="java&#45;quote">"&#45;Xdebug &#45;Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005"</span>
mvn grails:run&#45;app</pre></div><p class="paragraph"/><h4>Raising issues</h4><p class="paragraph"/>If you come across any problems with the Maven integration, please raise a JIRA issue as a sub-task of <a href="http://jira.codehaus.org/browse/GRAILS-3547" target="blank">GRAILS-3547</a>.

        </div>
    </body>
</html>
