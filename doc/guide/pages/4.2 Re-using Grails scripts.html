<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <title>4.2 Re-using Grails scripts</title>
        <link rel="stylesheet" href="../../css/main.css" type="text/css" media="screen" title="Ref" charset="utf-8"/>
        <link rel="icon" href="../../img/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon"/>
    </head>
    <body class="body">
        <div id="header">
            <h2><a name="4.2 Re-using Grails scripts">4.2 Re-using Grails scripts</a></h2>
        </div>
        <div id="toc">
            
        </div>
        <div id="content">
            Grails ships with a lot of command line functionality out of the box that you may find useful in your own scripts (See the command line reference in the reference guide for info on all the commands). Of particular use are the <a href="../ref/Command Line/compile.html" class="commandLine">compile</a>, <a href="../ref/Command Line/package.html" class="commandLine">package</a> and <a href="../ref/Command Line/bootstrap.html" class="commandLine">bootstrap</a> scripts.<p class="paragraph"/>The <a href="../ref/Command Line/bootstrap.html" class="commandLine">bootstrap</a> script for example allows you to bootstrap a Spring <a href="http://static.springsource.org/spring/docs/3.0.x/javadoc-api/org/springframework/context/ApplicationContext.html" class="api">ApplicationContext</a> instance to get access to the data source and so on (the integration tests use this):<p class="paragraph"/><div class="code"><pre>includeTargets &#60;&#60; grailsScript(<span class="java&#45;quote">"_GrailsBootstrap"</span>)<p class="paragraph"/>target ('<span class="java&#45;keyword">default</span>': <span class="java&#45;quote">"Load the Grails interactive shell"</span>) &#123;
	depends( configureProxy, packageApp, classpath, loadApp, configureApp )<p class="paragraph"/>	Connection c 
	<span class="java&#45;keyword">try</span> &#123;
		// <span class="java&#45;keyword">do</span> something with connection
		c = appCtx.getBean('dataSource').getConnection()
	&#125;
	<span class="java&#45;keyword">finally</span> &#123;
		c?.close()
	&#125;
&#125;</pre></div><p class="paragraph"/><h3>Pulling in targets from other scripts</h3><p class="paragraph"/>Gant allows you to pull in all targets (except "default") from another Gant script. You can then depend upon or invoke those targets as if they had been defined in the current script. The mechanism for doing this is the <code>includeTargets</code> property. Simply "append" a file or class to it using the left-shift operator:
<div class="code"><pre>includeTargets &#60;&#60; <span class="java&#45;keyword">new</span> File(<span class="java&#45;quote">"/path/to/my/script.groovy"</span>)
includeTargets &#60;&#60; gant.tools.Ivy</pre></div>
Don't worry too much about the syntax using a class, it's quite specialised. If you're interested, look into the Gant documentation.<p class="paragraph"/><h3>Core Grails targets</h3><p class="paragraph"/>As you saw in the example at the beginning of this section, you use neither the File- nor the class-based syntax for <code>includeTargets</code> when including core Grails targets. Instead, you should use the special <code>grailsScript()</code> method that is provided by the Grails command launcher (note that this is not available in normal Gant scripts, just Grails ones).<p class="paragraph"/>The syntax for the <code>grailsScript()</code> method is pretty straightforward: simply pass it the name of the Grails script you want to include, without any path information. Here is a list of Grails scripts that you may want to re-use:
<table class="wiki-table" cellpadding="0" cellspacing="0" border="0"><tr><th><strong class="bold">Script</strong></th><th><strong class="bold">Description</strong></th></tr><tr class="table-odd"><td>&#95;GrailsSettings</td><td>You really should include this! Fortunately, it is included automatically by all other Grails scripts bar one (&#95;GrailsProxy), so you usually don't have to include it explicitly.</td></tr><tr class="table-even"><td>&#95;GrailsEvents</td><td>If you want to fire events, you need to include this. Adds an <code>event(String eventName, List args)</code> method. Again, included by almost all other Grails scripts.</td></tr><tr class="table-odd"><td>&#95;GrailsClasspath</td><td>Sets up compilation, test, and runtime classpaths. If you want to use or play with them, include this script. Again, included by almost all other Grails scripts.</td></tr><tr class="table-even"><td>&#95;GrailsProxy</td><td>If you want to access the internet, include this script so that you don't run into problems with proxies.</td></tr><tr class="table-odd"><td>&#95;GrailsArgParsing</td><td>Provides a <code>parseArguments</code> target that does what it says on the tin: parses the arguments provided by the user when they run your script. Adds them to the <code>argsMap</code> property.</td></tr><tr class="table-even"><td>&#95;GrailsTest</td><td>Contains all the shared test code. Useful if you want to add any extra tests.</td></tr><tr class="table-odd"><td>&#95;GrailsRun</td><td>Provides all you need to run the application in the configured servlet container, either normally (<code>runApp</code>/<code>runAppHttps</code>) or from a WAR file (<code>runWar</code>/<code>runWarHttps</code>).</td></tr></table><p class="paragraph"/>There are many more scripts provided by Grails, so it is worth digging into the scripts themselves to find out what kind of targets are available. Anything that starts with an "_" is designed for re-use.<p class="paragraph"/> <blockquote class="note">
In pre-1.1 versions of Grails, the "_Grails..." scripts were not available. Instead, you typically include the corresponding command script, for example "Init.groovy" or "Bootstrap.groovy".<p class="paragraph"/>Also, in pre-1.0.4 versions of Grails you cannot use the <code>grailsScript()</code> method. Instead, you must use <code>includeTargets &#60;&#60; new File(...)</code> and specify the script's location in full (i.e. $GRAILS_HOME/scripts).
 </blockquote><p class="paragraph"/><h3>Script architecture</h3><p class="paragraph"/>You maybe wondering what those underscores are doing in the names of the Grails scripts. That is Grails' way of determining that a script is  <em class="italic">internal</em> , or in other words that it has not corresponding "command". So you can't run "grails _grails-settings" for example. That is also why they don't have a default target.<p class="paragraph"/>Internal scripts are all about code sharing and re-use. In fact, we recommend you take a similar approach in your own scripts: put all your targets into an internal script that can be easily shared, and provide simple command scripts that parse any command line arguments and delegate to the targets in the internal script. Say you have a script that runs some functional tests - you can split it like this:
<div class="code"><pre>./scripts/FunctionalTests.groovy:<p class="paragraph"/>includeTargets &#60;&#60; <span class="java&#45;keyword">new</span> File(<span class="java&#45;quote">"$&#123;basedir&#125;/scripts/_FunctionalTests.groovy"</span>)<p class="paragraph"/>target(<span class="java&#45;keyword">default</span>: <span class="java&#45;quote">"Runs the functional tests <span class="java&#45;keyword">for</span> <span class="java&#45;keyword">this</span> project."</span>) &#123;
    depends(runFunctionalTests)
&#125;<p class="paragraph"/>./scripts/_FunctionalTests.groovy:<p class="paragraph"/>includeTargets &#60;&#60; grailsScript(<span class="java&#45;quote">"_GrailsTest"</span>)<p class="paragraph"/>target(runFunctionalTests: <span class="java&#45;quote">"Run functional tests."</span>) &#123;
    depends(...)
    &#8230;
&#125;</pre></div><p class="paragraph"/>Here are a few general guidelines on writing scripts:
<ul class="star">
<li>Split scripts into a "command" script and an internal one.</li>
<li>Put the bulk of the implementation in the internal script.</li>
<li>Put argument parsing into the "command" script.</li>
<li>To pass arguments to a target, create some script variables and initialise them before calling the target.</li>
<li>Avoid name clashes by using closures assigned to script variables instead of targets. You can then pass arguments direct to the closures.</li>
</ul><p class="paragraph"/>
        </div>
    </body>
</html>
