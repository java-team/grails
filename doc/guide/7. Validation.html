<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <title>7. Validation</title>
        <link rel="stylesheet" href="../css/main.css" type="text/css" media="screen" title="Ref" charset="utf-8"/>
        <link rel="icon" href="../img/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon"/>
    </head>
    <body class="body">
        <div id="header">
            <h1><a name="7. Validation">7. Validation</a></h1>
        </div>
        <div id="toc">
            <div class="tocItem" style="margin-left:10px"><a href="#7.1 Declaring Constraints">7.1 Declaring Constraints</a></div><div class="tocItem" style="margin-left:10px"><a href="#7.2 Validating Constraints">7.2 Validating Constraints</a></div><div class="tocItem" style="margin-left:10px"><a href="#7.3 Validation on the Client">7.3 Validation on the Client</a></div><div class="tocItem" style="margin-left:10px"><a href="#7.4 Validation and Internationalization">7.4 Validation and Internationalization</a></div><div class="tocItem" style="margin-left:10px"><a href="#7.5 Validation Non Domain and Command Object Classes">7.5 Validation Non Domain and Command Object Classes</a></div>
        </div>
        <div id="content">
            Grails validation capability is built on <a href="http://static.springsource.org/spring/docs/3.0.x/javadoc-api/org/springframework/validation/package-summary.html" target="blank">Spring&#39;s Validator API</a> and data binding capabilities. However Grails takes this further and provides a unified way to define validation "constraints" with its constraints mechanism.<p class="paragraph"/>Constraints in Grails are a way to declaratively specify validation rules. Most commonly they are applied to <a href="../guide/single.html#5. Object Relational Mapping (GORM)" class="guide">domain classes</a>, however <a href="../guide/single.html#6.4 URL Mappings" class="guide">URL Mappings</a> and <a href="../guide/single.html#6.1.10 Command Objects" class="guide">Command Objects</a> also support constraints.<p class="paragraph"/><p class="paragraph"/><h2><a name="7.1 Declaring Constraints">7.1 Declaring Constraints</a></h2>Within a domain class a <a href="../ref/Domain Classes/constraints.html" class="domainClasses">constraints</a> are defined with the constraints property that is assigned a code block:<p class="paragraph"/><div class="code"><pre>class User &#123;
    <span class="java&#45;object">String</span> login
    <span class="java&#45;object">String</span> password
    <span class="java&#45;object">String</span> email
    <span class="java&#45;object">Integer</span> age<p class="paragraph"/>    <span class="java&#45;keyword">static</span> constraints = &#123;
	  &#8230;
    &#125;
&#125;</pre></div><p class="paragraph"/>You then use method calls that match the property name for which the constraint applies in combination with named parameters to specify constraints:<p class="paragraph"/>
<div class="code"><pre>class User &#123;
    ...<p class="paragraph"/>    <span class="java&#45;keyword">static</span> constraints = &#123;
        login(size:5..15, blank:<span class="java&#45;keyword">false</span>, unique:<span class="java&#45;keyword">true</span>)
        password(size:5..15, blank:<span class="java&#45;keyword">false</span>)
        email(email:<span class="java&#45;keyword">true</span>, blank:<span class="java&#45;keyword">false</span>)
        age(min:18, nullable:<span class="java&#45;keyword">false</span>)
    &#125;
&#125;</pre></div><p class="paragraph"/>In this example we've declared that the <code>login</code> property must be between 5 and 15 characters long, it cannot be blank and must be unique. We've all applied other constraints to the <code>password</code>, <code>email</code> and <code>age</code> properties.<p class="paragraph"/>A complete reference for the available constraints can be found in the left navigation bar (if you have frames enabled) under the Constraints heading.<p class="paragraph"/><h3>A word of warning - referencing domain class properties from constraints</h3><p class="paragraph"/>It's very easy to reference instance variables from the static constraints block, but this isn't legal in Groovy (or Java). If you do so, you will get a <code>MissingPropertyException</code> for your trouble. For example, you may try
<div class="code"><pre>class Response &#123;
    Survey survey
    Answer answer<p class="paragraph"/>    <span class="java&#45;keyword">static</span> constraints = &#123;
        survey blank: <span class="java&#45;keyword">false</span>
        answer blank: <span class="java&#45;keyword">false</span>, inList: survey.answers
    &#125;
&#125;</pre></div><p class="paragraph"/>See how the <code>inList</code> constraint references the instance property <code>survey</code>? That won't work. Instead, use a custom <a href="../ref/Constraints/validator.html" class="constraints">validator</a>:<p class="paragraph"/><div class="code"><pre>class Response &#123;
    &#8230;
    <span class="java&#45;keyword">static</span> constraints = &#123;
        survey blank: <span class="java&#45;keyword">false</span>
        answer blank: <span class="java&#45;keyword">false</span>, validator: &#123; val, obj &#45;&#62; val in obj.survey.answers &#125;
    &#125;
&#125;</pre></div><p class="paragraph"/>In this example, the <code>obj</code> argument to the custom validator is the domain  <em class="italic">instance</em>  that is being validated, so we can access its <code>survey</code> property and return a boolean to indicate whether the new value for the <code>answer</code> property, <code>val</code>, is valid or not.
<h2><a name="7.2 Validating Constraints">7.2 Validating Constraints</a></h2><h4>Validation Basics</h4><p class="paragraph"/>To validate a domain class you can call the <a href="../ref/Domain Classes/validate.html" class="domainClasses">validate</a> method on any instance:<p class="paragraph"/><div class="code"><pre>def user =  <span class="java&#45;keyword">new</span> User(params)<p class="paragraph"/><span class="java&#45;keyword">if</span>(user.validate()) &#123;
    // <span class="java&#45;keyword">do</span> something with user
&#125;
<span class="java&#45;keyword">else</span> &#123;
    user.errors.allErrors.each &#123;
        println it
    &#125;
&#125;</pre></div><p class="paragraph"/>The <code>errors</code> property on domain classes is an instance of the Spring <a href="http://static.springsource.org/spring/docs/3.0.x/javadoc-api/org/springframework/validation/Errors.html" class="api">Errors</a> interface. The <code>Errors</code> interface provides methods to navigate the validation errors and also retrieve the original values.<p class="paragraph"/><h4>Validation Phases</h4><p class="paragraph"/>Within Grails there are essentially 2 phases of validation, the first phase is <a href="../guide/single.html#6.1.6 Data Binding" class="guide">data binding</a> which occurs when you bind request parameters onto an instance such as:<p class="paragraph"/><div class="code"><pre>def user = <span class="java&#45;keyword">new</span> User(params)</pre></div><p class="paragraph"/>At this point you may already have errors in the <code>errors</code> property due to type conversion (such as converting Strings to Dates). You can check these and obtain the original input value using the <code>Errors</code> API:<p class="paragraph"/><div class="code"><pre><span class="java&#45;keyword">if</span>(user.hasErrors()) &#123;
	<span class="java&#45;keyword">if</span>(user.errors.hasFieldErrors(<span class="java&#45;quote">"login"</span>)) &#123;
		println user.errors.getFieldError(<span class="java&#45;quote">"login"</span>).rejectedValue
	&#125;
&#125;</pre></div><p class="paragraph"/>The second phase of validation happens when you call <a href="../ref/Domain Classes/validate.html" class="domainClasses">validate</a> or <a href="../ref/Domain Classes/save.html" class="domainClasses">save</a>. This is when Grails will validate the bound values againts the <a href="../ref/Domain Classes/constraints.html" class="domainClasses">constraints</a> you defined. For example, by default the persistent <a href="../ref/Domain Classes/save.html" class="domainClasses">save</a> method calls <code>validate</code> before executing hence allowing you to write code like:<p class="paragraph"/><div class="code"><pre><span class="java&#45;keyword">if</span>(user.save()) &#123;
    <span class="java&#45;keyword">return</span> user
&#125;
<span class="java&#45;keyword">else</span> &#123;
    user.errors.allErrors.each &#123;
        println it
    &#125;
&#125;</pre></div><p class="paragraph"/><h2><a name="7.3 Validation on the Client">7.3 Validation on the Client</a></h2><h4>Displaying Errors</h4><p class="paragraph"/>Typically if you get a validation error you want to redirect back to the view for rendering. Once there you need some way of rendering errors. Grails supports a rich set of tags for dealing with errors. If you simply want to render the errors as a list you can use <a href="../ref/Tags/renderErrors.html" class="tags">renderErrors</a>:<p class="paragraph"/><div class="code"><pre><span class="xml&#45;tag">&#60;g:renderErrors bean=<span class="xml&#45;quote">"$&#123;user&#125;"</span> /&#62;</span></pre></div><p class="paragraph"/>If you need more control you can use <a href="../ref/Tags/hasErrors.html" class="tags">hasErrors</a> and <a href="../ref/Tags/eachError.html" class="tags">eachError</a>:<p class="paragraph"/><div class="code"><pre><span class="xml&#45;tag">&#60;g:hasErrors bean=<span class="xml&#45;quote">"$&#123;user&#125;"</span>&#62;</span>
  <span class="xml&#45;tag">&#60;ul&#62;</span>
   <span class="xml&#45;tag">&#60;g:eachError var=<span class="xml&#45;quote">"err"</span> bean=<span class="xml&#45;quote">"$&#123;user&#125;"</span>&#62;</span>
       <span class="xml&#45;tag">&#60;li&#62;</span>$&#123;err&#125;<span class="xml&#45;tag">&#60;/li&#62;</span> 
   <span class="xml&#45;tag">&#60;/g:eachError&#62;</span>
  <span class="xml&#45;tag">&#60;/ul&#62;</span>
<span class="xml&#45;tag">&#60;/g:hasErrors&#62;</span></pre></div><p class="paragraph"/><h4>Highlighting Errors</h4><p class="paragraph"/>It is often useful to highlight using a red box or some indicator when a field has been incorrectly input. This can also be done with the <a href="../ref/Tags/hasErrors.html" class="tags">hasErrors</a> by invoking it as a method. For example:<p class="paragraph"/><div class="code"><pre><span class="xml&#45;tag">&#60;div class='value $&#123;hasErrors(bean:user,field:'login','errors')&#125;'&#62;</span>
   <span class="xml&#45;tag">&#60;input type=<span class="xml&#45;quote">"text"</span> name=<span class="xml&#45;quote">"login"</span> value=<span class="xml&#45;quote">"$&#123;fieldValue(bean:user,field:'login')&#125;"</span>/&#62;</span>
<span class="xml&#45;tag">&#60;/div&#62;</span></pre></div><p class="paragraph"/>What this code  does is check if the <code>login</code> field of the <code>user</code> bean has any errors and if it does adds an <code>errors</code> CSS class to the <code>div</code> thus allowing you to use CSS rules to highlight the <code>div</code>.<p class="paragraph"/>
<h4>Retrieving Input Values</h4><p class="paragraph"/>Each error is actually an instance of the <a href="http://static.springsource.org/spring/docs/3.0.x/javadoc-api/org/springframework/validation/FieldError.html" class="api">FieldError</a> class in Spring, which retains the original input value within it. This is useful as you can use the error object to restore the value input by the user using the <a href="../ref/Tags/fieldValue.html" class="tags">fieldValue</a> tag:<p class="paragraph"/><div class="code"><pre><span class="xml&#45;tag">&#60;input type=<span class="xml&#45;quote">"text"</span> name=<span class="xml&#45;quote">"login"</span> value=<span class="xml&#45;quote">"$&#123;fieldValue(bean:user,field:'login')&#125;"</span>/&#62;</span></pre></div><p class="paragraph"/>This code will look if there is an existing <code>FieldError</code> in the <code>User</code> bean and if there is obtain the originally input value for the <code>login</code> field.<h2><a name="7.4 Validation and Internationalization">7.4 Validation and Internationalization</a></h2>Another important thing to note about errors in Grails is that the messages that the errors display are not hard coded anywhere. The <a href="http://static.springsource.org/spring/docs/3.0.x/javadoc-api/org/springframework/validation/FieldError.html" class="api">FieldError</a> class in Spring essentially resolves messages from message bundles using Grails' <a href="../guide/single.html#10. Internationalization" class="guide">i18n</a> support.<p class="paragraph"/><h4>Constraints and Message Codes</h4><p class="paragraph"/>The codes themselves are dictated by a convention. For example consider the constraints we looked at earlier:<p class="paragraph"/><div class="code"><pre><span class="java&#45;keyword">package</span> com.mycompany.myapp<p class="paragraph"/>class User &#123;
    ...<p class="paragraph"/>    <span class="java&#45;keyword">static</span> constraints = &#123;
        login(size:5..15, blank:<span class="java&#45;keyword">false</span>, unique:<span class="java&#45;keyword">true</span>)
        password(size:5..15, blank:<span class="java&#45;keyword">false</span>)
        email(email:<span class="java&#45;keyword">true</span>, blank:<span class="java&#45;keyword">false</span>)
        age(min:18, nullable:<span class="java&#45;keyword">false</span>)
    &#125;
&#125;</pre></div><p class="paragraph"/>If the <code>blank</code> constraint was violated Grails will, by convention, look for a message code in the form:<p class="paragraph"/><div class="code"><pre>&#91;<span class="java&#45;object">Class</span> Name&#93;.&#91;Property Name&#93;.&#91;Constraint Code&#93;</pre></div><p class="paragraph"/>In the case of the <code>blank</code> constraint this would be <code>user.login.blank</code> so you would need a message such as the following in your <code>grails-app/i18n/messages.properties</code> file:<p class="paragraph"/><div class="code"><pre>user.login.blank=Your login name must be specified!</pre></div><p class="paragraph"/>The class name is looked for both with and without a package, with the packaged version taking precedence. So for example, com.mycompany.myapp.User.login.blank will be used before user.login.blank. This allows for cases where you domain class encounters message code clashes with plugins.<p class="paragraph"/>For a reference on what codes are for which constraints refer to the reference guide for each constraint.<p class="paragraph"/><h4>Displaying Messages</h4><p class="paragraph"/>The <a href="../ref/Tags/renderErrors.html" class="tags">renderErrors</a> tag will automatically deal with looking up messages for you using the <a href="../ref/Tags/message.html" class="tags">message</a> tag. However, if you need more control of rendering you will need to do this yourself:<p class="paragraph"/><div class="code"><pre><span class="xml&#45;tag">&#60;g:hasErrors bean=<span class="xml&#45;quote">"$&#123;user&#125;"</span>&#62;</span>
  <span class="xml&#45;tag">&#60;ul&#62;</span>
   <span class="xml&#45;tag">&#60;g:eachError var=<span class="xml&#45;quote">"err"</span> bean=<span class="xml&#45;quote">"$&#123;user&#125;"</span>&#62;</span>
       <span class="xml&#45;tag">&#60;li&#62;</span><span class="xml&#45;tag">&#60;g:message error=<span class="xml&#45;quote">"$&#123;err&#125;"</span> /&#62;</span><span class="xml&#45;tag">&#60;/li&#62;</span> 
   <span class="xml&#45;tag">&#60;/g:eachError&#62;</span>
  <span class="xml&#45;tag">&#60;/ul&#62;</span>
<span class="xml&#45;tag">&#60;/g:hasErrors&#62;</span></pre></div><p class="paragraph"/>In this example within the body of the <a href="../ref/Tags/eachError.html" class="tags">eachError</a> tag we use the <a href="../ref/Tags/message.html" class="tags">message</a> tag in combination with its <code>error</code> argument to read the message for the given error.<p class="paragraph"/><h2><a name="7.5 Validation Non Domain and Command Object Classes">7.5 Validation Non Domain and Command Object Classes</a></h2><a href="../guide/single.html#5.2 Domain Modelling in GORM" class="guide">Domain classes</a> and <a href="../guide/single.html#6.1.10 Command Objects" class="guide">command objects</a> support validation by default.  Other classes may be made validateable by defining the static constraints property in the class (as described above) and then telling the framework about them.  It is important that the application register the validateable classes with the framework.  Simply defining the constraints property is not sufficient.<p class="paragraph"/><h4>The Validateable Annotation</h4><p class="paragraph"/>Classes which define the static constraints property and are marked with the @Validateable annotation may be made validateable by the framework.  Consider this example:<p class="paragraph"/><div class="code"><pre>// src/groovy/com/mycompany/myapp/User.groovy
<span class="java&#45;keyword">package</span> com.mycompany.myapp<p class="paragraph"/><span class="java&#45;keyword">import</span> org.codehaus.groovy.grails.validation.Validateable<p class="paragraph"/>@Validateable
class User &#123;
    ...<p class="paragraph"/>    <span class="java&#45;keyword">static</span> constraints = &#123;
        login(size:5..15, blank:<span class="java&#45;keyword">false</span>, unique:<span class="java&#45;keyword">true</span>)
        password(size:5..15, blank:<span class="java&#45;keyword">false</span>)
        email(email:<span class="java&#45;keyword">true</span>, blank:<span class="java&#45;keyword">false</span>)
        age(min:18, nullable:<span class="java&#45;keyword">false</span>)
    &#125;
&#125;</pre></div><p class="paragraph"/>You need to tell the framework which packages to search for @Validateable classes by assigning a list of Strings to the grails.validateable.packages property in Config.groovy.<p class="paragraph"/><div class="code"><pre>// grails&#45;app/conf/Config.groovy<p class="paragraph"/>...<p class="paragraph"/>grails.validateable.packages = &#91;'com.mycompany.dto', 'com.mycompany.util'&#93;<p class="paragraph"/>...</pre></div><p class="paragraph"/>The framework will only search those packages (and child packages of those) for classes marked with @Validateable.<p class="paragraph"/><h4>Registering Validateable Classes</h4><p class="paragraph"/>If a class is not marked with @Validateable, it may still be made validateable by the framework.  The steps required to do this are to define the static constraints property in the class (as described above) and then telling the framework about the class by assigning a value to the grails.validateable.classes property in Config.groovy.<p class="paragraph"/>
<div class="code"><pre>// grails&#45;app/conf/Config.groovy<p class="paragraph"/>...<p class="paragraph"/>grails.validateable.classes = &#91;com.mycompany.myapp.User, com.mycompany.dto.Account&#93;<p class="paragraph"/>...</pre></div><p class="paragraph"/>
        </div>
    </body>
</html>
